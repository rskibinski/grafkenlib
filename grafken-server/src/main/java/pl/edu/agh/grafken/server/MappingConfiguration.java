package pl.edu.agh.grafken.server;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import pl.edu.agh.grafken.commons.ObjectMetaInfo;
import pl.edu.agh.grafken.lib.api.user.IObjectMetaInformation;
import pl.edu.agh.grafken.lib.api.user.IObjectMetaReference;
import pl.edu.agh.grafken.lib.database.exceptions.NotSuchDatabaseInitializedException;

@Path("mapping")
public class MappingConfiguration {

	@Path("getMapping/{dbId}")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getMappingForDatabase(@PathParam("dbId") String dbId) {
		try {
			return Server.getGrafken().getDatabasesManager()
					.getEmptyMappingFileForDatabase(dbId);
		} catch (NotSuchDatabaseInitializedException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Path("getObjectsList/{dbId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<ObjectMetaInfo> getUserObjectsList(
			@PathParam("dbId") String dbId) {
		Map<String, IObjectMetaInformation> objectsMeta = Server.getGrafken()
				.getMappingManager().getMappingForDatabase(dbId)
				.getObjectsMeta();
		ArrayList<ObjectMetaInfo> objectsMetaInfoList = new ArrayList<>();
		for (IObjectMetaInformation meta : objectsMeta.values()) {
			String objectName = meta.getName();
			ArrayList<String> connectedObjects = new ArrayList<>();
			for (IObjectMetaReference objectReference : meta
					.getConnectedObjectsMetaInformation()) {
				connectedObjects.add(objectReference.getForwardReferenceMeta()
						.getName());
			}
			objectsMetaInfoList.add(new ObjectMetaInfo(objectName,
					new ArrayList<String>(meta.getFieldsNames()),
					connectedObjects));
		}
		return objectsMetaInfoList;

	}

	@Path("getObjectsByName/{dbId}/{objectId}/{quantity}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public void getObjectHolderForType(@PathParam("dbId") String dbId,
			@PathParam("objectId") String objectName,
			@PathParam("quantity") int quantity) {

	}
}

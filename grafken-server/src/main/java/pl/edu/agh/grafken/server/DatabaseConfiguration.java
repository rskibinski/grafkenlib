package pl.edu.agh.grafken.server;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import pl.edu.agh.grafken.lib.api.admin.IDatabaseConfigurator;
import pl.edu.agh.grafken.lib.api.admin.IDatabaseEntity;
import pl.edu.agh.grafken.lib.api.admin.IDatabaseManager;

/**
 * Database configuration REST API
 * 
 */
@Path("databases")
public class DatabaseConfiguration {
	private static final Logger LOG = Logger
			.getLogger(DatabaseConfiguration.class);

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("getConfiguration")
	public String getConfiguration() {
		IDatabaseManager databasesManager = Server.getGrafken()
				.getDatabasesManager();
		IDatabaseConfigurator configurator = databasesManager.getConfigurator();
		return configurator.dumpDatabaseConfiguration(databasesManager
				.getDatabases());
	}

	@POST
	@Path("reloadConfiguration")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response reloadConfiguration(String newConfiguration) {
		LOG.info(newConfiguration);
		return Response.ok().build();
		//TODO: Actually reload that configuration
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("getIdList")
	public ArrayList<String> getDatabasesIdList(){
		ArrayList<String> list = new ArrayList<String>();
		for(IDatabaseEntity db : Server.getGrafken().getDatabasesManager().getDatabases()){
			list.add(db.getId());
		}
		System.out.println(list);
		return list;
	}
}

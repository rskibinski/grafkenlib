package pl.edu.agh.grafken.server;

import java.io.IOException;
import java.net.URI;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

import pl.edu.agh.grafken.lib.GrafkenDaemon;

/**
 * Main class.
 *
 */
public class Server {
    // Base URI the Grizzly HTTP server will listen on
    public static final String BASE_URI = "http://localhost:8080/grafken/";
	private static GrafkenDaemon grafkenService;

    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {
        // create a resource config that scans for JAX-RS resources and providers
        // in pl.edu.agh.grafken.server package
        final ResourceConfig rc = new ResourceConfig().packages("pl.edu.agh.grafken.server");
        rc.register(JacksonFeature.class);
        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }
    
    public static GrafkenDaemon startGrafkenService(){
    	GrafkenDaemon grafkenDaemon = GrafkenDaemon.getInstance();
    	grafkenDaemon.init();
    	grafkenDaemon.startConsole();
    	return grafkenDaemon;
    	
    }

    /**
     * Main method.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
    	grafkenService = startGrafkenService();
        final HttpServer server = startServer();
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl\nHit enter to stop it...", BASE_URI));
        System.in.read();
        server.stop();
        grafkenService.shutdown();
    }
    
    public static GrafkenDaemon getGrafken(){
    	return grafkenService;
    }
}


package pl.edu.agh.grafken.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import pl.edu.agh.grafken.lib.api.user.IMapping;
import pl.edu.agh.grafken.lib.api.user.IObjectMetaInformation;
import pl.edu.agh.grafken.lib.api.user.IObjectMetaReference;
import pl.edu.agh.grafken.lib.api.user.IObjectsCache;
import pl.edu.agh.grafken.lib.api.user.IUserObject;
import pl.edu.agh.grafken.lib.database.exceptions.NotSuchDatabaseInitializedException;
import pl.edu.agh.grafken.lib.utils.KeyImmutableList;
import pl.edu.agh.grafken.lib.utils.KeyImmutableListKeyDeserializer;
import pl.edu.agh.grafken.server.wrapper.ObjectMetaInformationWrapper;
import pl.edu.agh.grafken.server.wrapper.ObjectsCacheWrapper;

import com.fasterxml.jackson.core.JsonProcessingException;

@Path("API/{mappingID}")
public class Api {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("getObjectTypesList")
	public ArrayList<ObjectMetaInformationWrapper> getObjecsTypesList(
			@PathParam("mappingID") String mappingID) {
		Collection<IObjectMetaInformation> metas = Server.getGrafken()
				.getMappingManager().getMappingForDatabase(mappingID)
				.getObjectsMeta().values();
		ArrayList<ObjectMetaInformationWrapper> result = new ArrayList<>();
		for (IObjectMetaInformation meta : metas) {
			result.add(new ObjectMetaInformationWrapper(meta));
		}
		return result;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("getObjectData/{objectID}/{maxResultNumber}")
	public ObjectsCacheWrapper getObjectData(
			@PathParam("mappingID") String mappingID,
			@PathParam("objectID") String objectID,
			@PathParam("maxResultNumber") String maxResultNumber) {
		int resultsNumber;
		try {
			resultsNumber = Integer.parseInt(maxResultNumber);
			if (resultsNumber <= 0)
				throw new NumberFormatException();
		} catch (NumberFormatException e) {
			resultsNumber = 50;
		}

		IMapping mapping = Server.getGrafken().getMappingManager()
				.getMappingForDatabase(mappingID);
		IObjectMetaInformation metaInformation = mapping
				.getObjectMetaInformation(objectID);
		IObjectsCache rawResults = metaInformation.getObjects(resultsNumber);
		//
		ObjectsCacheWrapper cacheWrapper = new ObjectsCacheWrapper(
				new ObjectMetaInformationWrapper(metaInformation), rawResults);
		return cacheWrapper;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("getObjectConnection/{objectType}/{objectKey}/{referenceName}")
	public ObjectsCacheWrapper getObjectConnections(
			@PathParam("mappingID") String mappingID,
			@PathParam("objectType") String objectType,
			@PathParam("objectKey") String objectKey,
			@PathParam("referenceName") String referenceName)
			throws JsonProcessingException, IOException {
		IMapping mapping = Server.getGrafken().getMappingManager()
				.getMappingForDatabase(mappingID);

		IObjectMetaInformation metaInformation = mapping
				.getObjectMetaInformation(objectType);
		IObjectMetaReference metaReference = metaInformation
				.getConnectedObjectMeta(referenceName);

		KeyImmutableListKeyDeserializer deserializer = new KeyImmutableListKeyDeserializer();
		@SuppressWarnings("unchecked")
		KeyImmutableList<String> key = (KeyImmutableList<String>) deserializer
				.deserializeKey(objectKey, null);

		IUserObject objectFromCache = metaInformation.getObjectsCache()
				.getObject(key);
		IObjectsCache result = metaReference
				.getObjectsConnected(objectFromCache);
		return new ObjectsCacheWrapper(new ObjectMetaInformationWrapper(
				metaReference.getForwardReferenceMeta()), result);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("search/{objectType}/{searchQuery}")
	public ObjectsCacheWrapper searchForObjects(
			@PathParam("mappingID") String mappingID,
			@PathParam("objectType") String objectType,
			@PathParam("searchQuery") String searchQuery) throws NotSuchDatabaseInitializedException {
		IMapping mapping = Server.getGrafken().getMappingManager()
				.getMappingForDatabase(mappingID);
		IObjectMetaInformation metaInformation = mapping
				.getObjectMetaInformation(objectType);
		
		IObjectsCache results = metaInformation.searchForObject(searchQuery);
		return new ObjectsCacheWrapper(new ObjectMetaInformationWrapper(metaInformation), results);
	}

}

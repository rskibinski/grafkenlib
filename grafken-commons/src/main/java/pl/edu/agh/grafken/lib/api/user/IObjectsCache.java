package pl.edu.agh.grafken.lib.api.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import pl.edu.agh.grafken.lib.utils.KeyImmutableList;
import pl.edu.agh.grafken.lib.wrapper.interfaces.IObjectsCacheWrapper;

public interface IObjectsCache extends IObjectsCacheWrapper{
	
	public ArrayList<IUserObject> getAllObjects();

	public void addObjects(Collection<IUserObject> allObjects);
	
	public IObjectMetaInformation getMeta();

	void add(IUserObject object);

	IUserObject getObject(KeyImmutableList<String> key);
	
	public Set<KeyImmutableList<String>> getKeys();

	public Set<IUserObject> getObjectsSet(Set<KeyImmutableList<String>> keys);
	
	public IObjectsCache getObjectsSubCache(Set<KeyImmutableList<String>> keys);
}
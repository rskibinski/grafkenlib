package pl.edu.agh.grafken.lib.api.admin;

import pl.edu.agh.grafken.lib.api.user.IMapping;

public interface IMappingManager {

	public void addMapping(IMapping mm);

	public IMapping getMappingForDatabase(String databaseName);

	public abstract void reset();
}
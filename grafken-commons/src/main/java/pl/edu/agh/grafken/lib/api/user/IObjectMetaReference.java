package pl.edu.agh.grafken.lib.api.user;




public interface IObjectMetaReference {	
	
	public String getForeignTableName();
	
	public String getForeignColumnName();
	
	public String getOriginTableName();
	
	public String getOriginColumnName();
	
	public IObjectMetaInformation getForwardReferenceMeta();
	
	public IObjectMetaInformation getBackwardReferenceMeta();
	
	public IObjectsCache getObjectsConnected(IUserObject userObject);
	
}

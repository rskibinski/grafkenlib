package pl.edu.agh.grafken.lib.utils;

import java.util.ArrayList;

import pl.edu.agh.grafken.lib.wrapper.interfaces.IObjectMetaInformationWrapper;
import pl.edu.agh.grafken.lib.wrapper.interfaces.IObjectsCacheWrapper;
import pl.edu.agh.grafken.lib.wrapper.interfaces.IUserObjectWrapper;
import dnl.utils.text.table.TextTable;

public class PrintUtils {

	public static void prettyPrintCache(IObjectsCacheWrapper results){
		ArrayList<? extends IUserObjectWrapper> allObjects = results.getAllObjects();
		IObjectMetaInformationWrapper meta = results.getMeta();
		String header[] = new String[meta.getFieldsNames().size()];
		header = meta.getFieldsNames().toArray(header);
		TextTable tt = new TextTable(header,
				ArrayUtils.createArray2d(allObjects));
		tt.setAddRowNumbering(true);
		tt.printTable();
	}
}

package pl.edu.agh.grafken.lib.wrapper.interfaces;

import java.util.ArrayList;
import java.util.Set;

import pl.edu.agh.grafken.lib.utils.KeyImmutableList;

public interface IObjectsCacheWrapper {

	public IObjectMetaInformationWrapper getMeta();

	public ArrayList<? extends IUserObjectWrapper> getAllObjects();

	public IUserObjectWrapper getObject(KeyImmutableList<String> key);

	public Set<? extends IUserObjectWrapper> getObjectsSet(
			Set<KeyImmutableList<String>> keys);

	public IObjectsCacheWrapper getObjectsSubCache(
			Set<KeyImmutableList<String>> keys);
}

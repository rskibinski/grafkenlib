package pl.edu.agh.grafken.server.wrapper;

import java.util.ArrayList;
import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonIgnore;

import pl.edu.agh.grafken.lib.api.user.IObjectMetaInformation;
import pl.edu.agh.grafken.lib.wrapper.interfaces.IObjectMetaInformationWrapper;
import pl.edu.agh.grafken.lib.wrapper.interfaces.IObjectsCacheWrapper;

public class ObjectMetaInformationWrapper implements
		IObjectMetaInformationWrapper {

	private String name;
	private Collection<String> fieldsNames = new ArrayList<>();
	private Collection<String> connectedObjectNames = new ArrayList<>();
	@JsonIgnore
	private ObjectsCacheWrapper objectsCache = new ObjectsCacheWrapper();

	public ObjectMetaInformationWrapper() {
	}

	public ObjectMetaInformationWrapper(IObjectMetaInformation in) {
		this.name = in.getName();
		this.fieldsNames = in.getFieldsNames();
		this.connectedObjectNames = in.getConnectedObjectsNames();
		this.objectsCache = new ObjectsCacheWrapper(this, in.getObjectsCache());
	}

	@Override
	public Collection<String> getFieldsNames() {
		return fieldsNames;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Collection<String> getConnectedObjectsNames() {
		return connectedObjectNames;
	}

	@Override
	public IObjectsCacheWrapper getObjectsCache() {
		return objectsCache;
	}

}

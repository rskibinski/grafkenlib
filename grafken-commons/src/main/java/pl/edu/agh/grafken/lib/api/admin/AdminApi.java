package pl.edu.agh.grafken.lib.api.admin;



public interface AdminApi {
	
	public IDatabaseManager getDatabasesManager();

	public abstract void init();

}
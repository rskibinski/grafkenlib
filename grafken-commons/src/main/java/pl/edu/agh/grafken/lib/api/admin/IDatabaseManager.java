package pl.edu.agh.grafken.lib.api.admin;

import java.util.ArrayList;

import pl.edu.agh.grafken.lib.database.exceptions.ConfigurationParsingException;
import pl.edu.agh.grafken.lib.database.exceptions.NotSuchDatabaseInitializedException;

public interface IDatabaseManager {
	/**
	 * Read configuration file and initialize connection pools.
	 * 
	 * @throws ConfigurationParsingException
	 */
	public abstract void initializeDatabases()
			throws ConfigurationParsingException;

	/**
	 * Returns database entity if it's recognizable by name.
	 * 
	 * @param name
	 *            unique id, read from conf file
	 * @return {@link IDatabaseEntity} instance
	 * @throws NotSuchDatabaseInitializedException
	 */
	public abstract IDatabaseEntity getDatabase(String name)
			throws NotSuchDatabaseInitializedException;

	/**
	 * Returns list of all {@link IDatabaseEntity} initilized in app instance.
	 * 
	 * @return {@link IDatabaseEntity} list
	 */
	public abstract ArrayList<IDatabaseEntity> getDatabases();

	/**
	 * Gets default (from configurator) mapping file path for database of given
	 * name.
	 * 
	 * @param databaseName
	 * @return mapping file path
	 */
	public abstract String getDefaultMappingFilePathForDatabase(
			String databaseName);
	
	/**
	 * Returns {@link IDatabaseConfigurator}.
	 */
	public abstract IDatabaseConfigurator getConfigurator();
	
	public abstract String getEmptyMappingFileForDatabase(String databaseName) throws NotSuchDatabaseInitializedException;
}
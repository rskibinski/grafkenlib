package pl.edu.agh.grafken.lib.utils;

public class CustomStringBuilder {

	private final StringBuilder sb;

	private static final String NEW_LINE = "\n";

	public CustomStringBuilder() {
		sb = new StringBuilder();
	}

	public StringBuilder appendLine(String text) {
		return sb.append(text).append(NEW_LINE);
	}

	public String toString() {
		return sb.toString();
	}
}

package pl.edu.agh.grafken.commons;

import java.util.ArrayList;

public class ObjectMetaInfo {
	private String objectName;
	private ArrayList<String> fieldNames;
	private ArrayList<String> connectedObjectsNames;

	public ObjectMetaInfo() {
		fieldNames = new ArrayList<>();
		connectedObjectsNames = new ArrayList<>();
	}

	public ObjectMetaInfo(String objectName, ArrayList<String> fieldNames,
			ArrayList<String> connectedObjectsNames) {
		this.objectName = objectName;
		this.fieldNames = fieldNames;
		this.connectedObjectsNames = connectedObjectsNames;
	}

	public ArrayList<String> getFieldNames() {
		return fieldNames;
	}

	public void setFieldNames(ArrayList<String> fieldNames) {
		this.fieldNames = fieldNames;
	}

	public ArrayList<String> getConnectedObjectsNames() {
		return connectedObjectsNames;
	}

	public void setConnectedObjectsNames(ArrayList<String> connectedObjectsNames) {
		this.connectedObjectsNames = connectedObjectsNames;
	}

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

}

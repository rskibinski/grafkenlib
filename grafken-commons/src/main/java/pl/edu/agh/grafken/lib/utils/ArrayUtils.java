package pl.edu.agh.grafken.lib.utils;

import java.util.ArrayList;

import pl.edu.agh.grafken.lib.wrapper.interfaces.IUserObjectWrapper;

public class ArrayUtils {

	public static Object[][] flattenArray2d(
			ArrayList<ArrayList<String>> allObjects) {
		Object[][] outer = new Object[allObjects.size()][];
		for (int i = 0; i < allObjects.size(); i++) {
			outer[i] = allObjects.get(i).toArray();
		}
		return outer;
	}

	public static Object[][] createArray2d(ArrayList<? extends IUserObjectWrapper> allObjects) {
		Object[][] outer = new Object[allObjects.size()][];
		for (int i = 0; i < allObjects.size(); i++) {
			outer[i] = allObjects.get(i).getValuesList().toArray();
		}
		return outer;
	}
}

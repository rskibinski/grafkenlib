package pl.edu.agh.grafken.server.wrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import org.apache.commons.lang.NotImplementedException;

import pl.edu.agh.grafken.lib.api.user.IObjectsCache;
import pl.edu.agh.grafken.lib.api.user.IUserObject;
import pl.edu.agh.grafken.lib.utils.KeyImmutableList;
import pl.edu.agh.grafken.lib.utils.KeyImmutableListKeyDeserializer;
import pl.edu.agh.grafken.lib.wrapper.interfaces.IObjectMetaInformationWrapper;
import pl.edu.agh.grafken.lib.wrapper.interfaces.IObjectsCacheWrapper;
import pl.edu.agh.grafken.lib.wrapper.interfaces.IUserObjectWrapper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonAutoDetect
public class ObjectsCacheWrapper implements IObjectsCacheWrapper {

	private ObjectMetaInformationWrapper meta;
	@JsonProperty
	@JsonDeserialize(keyUsing = KeyImmutableListKeyDeserializer.class)
	private HashMap<KeyImmutableList<String>, UserObjectWrapper> cache = new HashMap<>();

	public ObjectsCacheWrapper() {
	}

	public ObjectsCacheWrapper(ObjectMetaInformationWrapper meta,
			IObjectsCache in) {
		this.meta = meta;
		for (IUserObject object : in.getAllObjects()) {
			cache.put(object.getKey(), new UserObjectWrapper(meta, object));
		}
	}

	@Override
	public IObjectMetaInformationWrapper getMeta() {
		return this.meta;
	}

	@JsonIgnore
	@Override
	public ArrayList<? extends UserObjectWrapper> getAllObjects() {
		return new ArrayList<UserObjectWrapper>(cache.values());
	}

	@Override
	public IUserObjectWrapper getObject(KeyImmutableList<String> key) {
		return cache.get(key);
	}

	@Override
	public Set<? extends IUserObjectWrapper> getObjectsSet(
			Set<KeyImmutableList<String>> keys) {
		throw new NotImplementedException();
	}

	@Override
	public IObjectsCacheWrapper getObjectsSubCache(
			Set<KeyImmutableList<String>> keys) {
		throw new NotImplementedException();
	}

}

package pl.edu.agh.grafken.lib.database.schema.model;

/**
 * Interface that allows to get basic information about database column.
 * 
 */
public interface IDatabaseColumn {
	/**
	 * Returns name under which column is recognizable in database.
	 * 
	 * @return column name
	 */
	public String getColumnName();

	/**
	 * Returns mapping string to be processed.
	 * 
	 * @return mapping string
	 */
	public String getMapping();

	/**
	 * Returns table name in which column is in database.
	 * 
	 * @return column table name
	 */
	public String getTableName();

	/**
	 * Returns {@link DatabaseTable} object associated with {@link IDatabaseColumn} object.
	 * 
	 * @return {@link IDatabaseTable} object.
	 */
	public IDatabaseTable getTable(); 

}

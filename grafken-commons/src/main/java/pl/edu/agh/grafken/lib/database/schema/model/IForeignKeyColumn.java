package pl.edu.agh.grafken.lib.database.schema.model;

/**
 * Foreign column name with information connected table and column, also
 * providing backward relation column name.
 * 
 * 
 */
public interface IForeignKeyColumn extends IDatabaseColumn {
	/**
	 * Returns foreign table database name.
	 * 
	 * @return table name
	 */
	public String getForeignTableName();

	/**
	 * Returns foreign table column name in database
	 * 
	 * @return column name
	 */
	public String getForeignTableColumn();

	/**
	 * Returns origin column name in databse.
	 * 
	 * @return origin column name
	 */
	public String getOriginColumnName();

}

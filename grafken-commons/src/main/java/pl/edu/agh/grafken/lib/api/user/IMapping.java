package pl.edu.agh.grafken.lib.api.user;

import java.util.Map;
import java.util.Set;

public interface IMapping {
	/**
	 * Returns all objects metainformation.
	 * 
	 * @return
	 */
	public Map<String, IObjectMetaInformation> getObjectsMeta();

	/**
	 * Returns meta information for object name.
	 * 
	 * @param objectName
	 * @return
	 */
	public IObjectMetaInformation getObjectMetaInformation(String objectName);

	/**
	 * Returns object names.
	 * 
	 * @return
	 */
	public Set<String> getUserObjectNames();

	/**
	 * Validate mapping with database.
	 */
	public boolean isValid();

	/**
	 * Returns database name
	 * 
	 * @return db name
	 */
	public String getDatabaseID();

}
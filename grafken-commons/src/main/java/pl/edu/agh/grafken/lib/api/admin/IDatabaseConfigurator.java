package pl.edu.agh.grafken.lib.api.admin;

import java.util.ArrayList;

import pl.edu.agh.grafken.lib.database.exceptions.ConfigurationParsingException;

public interface IDatabaseConfigurator {

	/**
	 * Parse file on default path and creates not initialized
	 * {@link DatabaseEntity} list.
	 * 
	 * @return List of uninitialized DatabaseEntities - Decorator for db.
	 * @throws ConfigurationParsingException
	 */
	public ArrayList<? extends IDatabaseEntity> parseFileForDatabaseConfigurations()
			throws ConfigurationParsingException;

	/**
	 * Parse file on given path and creates not initialized
	 * {@link DatabaseEntity} list.
	 * 
	 * @param filePath
	 *            configuration file path
	 * @return List of uninitialized DatabaseEntities - Decorator for db.
	 * @throws ConfigurationParsingException
	 */
	public ArrayList<? extends IDatabaseEntity> parseFileForDatabaseConfigurations(
			String filePath) throws ConfigurationParsingException;

	/**
	 * Dumps databases configuration to file.
	 * 
	 * @param dbs
	 *            list of {@link DatabaseEntity} - databases uses in app.
	 * @param filePath
	 *            where to dump a file
	 */
	public void dumpDatabaseConfiguration(ArrayList<IDatabaseEntity> dbs,
			String filePath);

	/**
	 * Returns default file path that should contain user mapping for database.
	 * 
	 * @param dbName
	 *            database name
	 * @return mapping configuration file path
	 */
	public String getDefaultMappingFilePath(String dbName);

	/**
	 * Returns default database configuration path
	 * 
	 * @return path to file with db configuration
	 */
	public String getConfigurationPath();

	/**
	 * Dumps databases configuration to String.
	 * 
	 * @param dbs
	 *            list of {@link DatabaseEntity} - databases uses in app.
	 * @return database configuration in string format.
	 */
	public String dumpDatabaseConfiguration(ArrayList<IDatabaseEntity> dbs);
}
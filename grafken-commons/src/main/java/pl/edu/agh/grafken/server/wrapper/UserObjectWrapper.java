package pl.edu.agh.grafken.server.wrapper;

import java.util.ArrayList;

import pl.edu.agh.grafken.lib.api.user.IUserObject;
import pl.edu.agh.grafken.lib.utils.KeyImmutableList;
import pl.edu.agh.grafken.lib.wrapper.interfaces.IObjectMetaInformationWrapper;
import pl.edu.agh.grafken.lib.wrapper.interfaces.IUserObjectWrapper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
@JsonAutoDetect
public class UserObjectWrapper implements IUserObjectWrapper {

	private ObjectMetaInformationWrapper metaInformation;
	private ArrayList<String> values = new ArrayList<>();
	private KeyImmutableList<String> key;

	public UserObjectWrapper() {
	}

	public UserObjectWrapper(ObjectMetaInformationWrapper meta,
			IUserObject object) {
		this.metaInformation = meta;
		this.values = object.getValuesList();
		key = object.getKey();
	}

	@Override
	public IObjectMetaInformationWrapper getMetaInformation() {
		return metaInformation;
	}

	@Override
	public ArrayList<String> getValuesList() {
		return values;
	}

	@Override
	public String getPropertyValue(int index) {
		return values.get(index);
	}

	@Override
	public KeyImmutableList<String> getKey() {
		return key;
	}

}

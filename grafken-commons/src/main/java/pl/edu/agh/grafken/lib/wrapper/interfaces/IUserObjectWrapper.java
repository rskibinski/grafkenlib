package pl.edu.agh.grafken.lib.wrapper.interfaces;

import java.util.ArrayList;

import pl.edu.agh.grafken.lib.utils.KeyImmutableList;

public interface IUserObjectWrapper {

	public IObjectMetaInformationWrapper getMetaInformation();

	/**
	 * Values should be ordered as in meta information connected with object
	 * instance.
	 * 
	 * @return
	 */
	public abstract ArrayList<String> getValuesList();

	public abstract String getPropertyValue(int index);

	/**
	 * Unique in term of one meta information object key
	 * 
	 * @return object uid
	 */
	public abstract KeyImmutableList<String> getKey();
}

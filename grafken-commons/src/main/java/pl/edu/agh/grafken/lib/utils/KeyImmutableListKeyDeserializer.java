package pl.edu.agh.grafken.lib.utils;

import java.io.IOException;

import pl.edu.agh.grafken.lib.utils.KeyImmutableList.Builder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;

public class KeyImmutableListKeyDeserializer extends KeyDeserializer {

	@Override
	public Object deserializeKey(String arg0, DeserializationContext arg1)
			throws IOException, JsonProcessingException {
		String[] split = arg0.split(",");
		Builder<String> builder = KeyImmutableList.builder();
		{
			for (String s : split) {
				builder.add(s);
			}
		}
		return builder.build();
	}

}

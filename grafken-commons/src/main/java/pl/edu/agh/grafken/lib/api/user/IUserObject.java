package pl.edu.agh.grafken.lib.api.user;

import java.util.ArrayList;

import pl.edu.agh.grafken.lib.wrapper.interfaces.IUserObjectWrapper;

public interface IUserObject extends IUserObjectWrapper {

	public abstract IObjectMetaInformation getMetaInformation();

	/**
	 * Values should be ordered as in meta information connected with object
	 * instance.
	 * 
	 * @return
	 */
	public abstract ArrayList<String> getForwardBindings();

}

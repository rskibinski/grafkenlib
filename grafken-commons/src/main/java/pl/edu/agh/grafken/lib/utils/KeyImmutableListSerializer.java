package pl.edu.agh.grafken.lib.utils;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class KeyImmutableListSerializer extends
		JsonSerializer<KeyImmutableList<Object>> {

	@Override
	public void serialize(KeyImmutableList<Object> value, JsonGenerator jgen,
			SerializerProvider arg2) throws IOException,
			JsonProcessingException {
		jgen.writeStartObject();
		jgen.writeFieldName("keys");
		jgen.writeStartArray();
		for (Object item : value.getList()) {
			jgen.writeString(item.toString());
		}
		jgen.writeEndArray();
		jgen.writeEndObject();
		
	}

}

package pl.edu.agh.grafken.lib.wrapper.interfaces;

import java.util.Collection;

public interface IObjectMetaInformationWrapper {

	/**
	 * Returns list of attributes defined for user object. Ordering is important!
	 * 
	 * @return
	 */
	public Collection<String> getFieldsNames();

	/**
	 * Returns user defined name for the object
	 * 
	 * @return object name
	 */
	public String getName();

	public Collection<String> getConnectedObjectsNames();
	
	public IObjectsCacheWrapper getObjectsCache();

}
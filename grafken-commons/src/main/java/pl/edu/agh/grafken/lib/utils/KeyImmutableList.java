package pl.edu.agh.grafken.lib.utils;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.ImmutableList;

@JsonAutoDetect
@JsonSerialize(using = KeyImmutableListSerializer.class)
@JsonDeserialize(using = KeyImmutableListDeserializer.class)
public class KeyImmutableList<E> {

	private static final Logger log = Logger.getLogger(KeyImmutableList.class);

	public static class Builder<E> {
		private ImmutableList.Builder<E> guavaBuilder = new ImmutableList.Builder<>();

		public KeyImmutableList<E> build() {
			KeyImmutableList<E> keyImmutableList = new KeyImmutableList<>();
			keyImmutableList.list = guavaBuilder.build();
			return keyImmutableList;
		}

		public ImmutableList.Builder<E> getGuavaBuilder() {
			return guavaBuilder;
		}

		public Builder<E> add(E value) {
			guavaBuilder.add(value);
			return this;
		}
	}

	private ImmutableList<E> list;

	private KeyImmutableList() {
	}

	public static <E> KeyImmutableList.Builder<E> builder() {
		return new Builder<E>();
	}

	public static <E> KeyImmutableList.Builder<E> builder(
			Iterable<? extends E> it) {
		Builder<E> builder = new Builder<E>();
		builder.guavaBuilder.addAll(it);
		return builder;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof KeyImmutableList)) { // or
			// !(this.type.isAssignableFrom(arg0.getClass())),
			// look :
			// http://stackoverflow.com/questions/1570073/java-instanceof-and-generics
			return false;
		}
		KeyImmutableList<E> arg0 = (KeyImmutableList<E>) obj;
		return this.hashCode() == arg0.hashCode();
	}

	@Override
	public int hashCode() {
		int result = 17;
		int i = 1;
		for (E obj : list) {
			result *= 31 + obj.hashCode() * i;
			i++;
		}
		return result;
	}

	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		for (E it : list) {
			b.append(it).append(",");
		}
		b.deleteCharAt(b.length() - 1);
		return b.toString();
	}

	@JsonIgnore
	public ImmutableList<E> getList() {
		return this.list;
	}

}

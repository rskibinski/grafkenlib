package pl.edu.agh.grafken.lib.api.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import pl.edu.agh.grafken.lib.database.exceptions.NotSuchDatabaseInitializedException;
import pl.edu.agh.grafken.lib.database.schema.model.IDatabaseColumn;
import pl.edu.agh.grafken.lib.wrapper.interfaces.IObjectMetaInformationWrapper;

/**
 * Interface describing methods used by front-end templates for proper rendering
 * data needed by user.
 * 
 */
public interface IObjectMetaInformation extends IObjectMetaInformationWrapper {
	/**
	 * Reference to mapping containing that meta.
	 * 
	 * @return parent reference
	 */
	public IMapping getParent();

	/**
	 * Adds object connected by reference with this one.
	 * 
	 * @param connectedObject
	 *            another {@link IUserObject)
	 */
	public void addConnectedObject(IObjectMetaReference objectConnected);

	/**
	 * Returns meta information about
	 * 
	 * @param objectMetaID
	 * @return
	 */
	public IObjectMetaReference getConnectedObjectMeta(String objectMetaID);

	/**
	 * Returns wrapper around another {@link IObjectMetaInformation} with
	 * additional reference informations.
	 * 
	 * @return connected objects meta information
	 */
	public Collection<IObjectMetaReference> getConnectedObjectsMetaInformation();

	/**
	 * Returns map of <Object field, referenced columns in db>
	 * 
	 * @return fields mapping for this meta
	 */
	public abstract Map<String, Collection<IDatabaseColumn>> getFieldsMapping();

	/**
	 * Sets object name
	 * 
	 * @param unique
	 *            name
	 */
	public void setName(String line);

	/**
	 * Add database column as a field.
	 * 
	 * @param column
	 * @param fieldName
	 * @param priority
	 */
	public void addColumnAsField(IDatabaseColumn column, String fieldName,
			int priority);

	/**
	 * Returns object cache that should contain all objects that were got from
	 * database and should be distinguish as java objects, not only by their
	 * key.
	 * 
	 * @return objects cache
	 */
	public IObjectsCache getObjectsCache();

	/**
	 * Sets object cache
	 * 
	 * @param objectsCache
	 *            object cache
	 */
	public void setObjectsCache(IObjectsCache objectsCache);

	/**
	 * Add db column, that should be used to create unique in term of database
	 * object.
	 * 
	 * @param column
	 *            db key column
	 */
	public abstract void addKeyColumn(IDatabaseColumn column);

	/**
	 * Wrapper on addKeyColumn.
	 * 
	 * @param columns
	 *            key columns collection
	 */
	public abstract void addKeyColumns(
			Collection<? extends IDatabaseColumn> columns);

	/**
	 * Because {@link IUserObject} may not contain meta information about
	 * references and store them in ordered data structures, reference index is
	 * needed to extract proper binding to retrieve objects' connected ones.
	 * 
	 * @param key
	 *            reference name
	 * @return index of reference
	 */
	public int getReferenceIndex(String key);

	/**
	 * Add reference of metaObjects that are pointing on this object.
	 * 
	 * @param objectReference
	 */
	public void addBackwardReference(IObjectMetaReference objectReference);

	/**
	 * Returns references pointing on this object.
	 * 
	 * @return
	 */
	Collection<IObjectMetaReference> getBackwardReferences();

	/**
	 * Return ordered columns(must not be unique) allowing for creating unique
	 * key for instance of {@link IUserObject}.
	 * 
	 * @return key columns
	 */
	public abstract ArrayList<IDatabaseColumn> getUidColumns();

	/**
	 * Perform easy search case on database: using given String value and LIKE
	 * operator on OR sum of all object fields.
	 * 
	 * @param value
	 *            String that should be compared with other fields
	 * @return Objects that satisfty search conditions
	 * @throws NotSuchDatabaseInitializedException
	 */
	public IObjectsCache searchForObject(String value)
			throws NotSuchDatabaseInitializedException;

	/**
	 * Returns first maxResultNumber of objects from db/cache.
	 * 
	 * @param maxResultNumber
	 * @return objects from db
	 */
	public IObjectsCache getObjects(int maxResultNumber);

	/**
	 * Returns maxResultNumber objects offseted from database.
	 * 
	 * @param maxResultNumber
	 *            max result number, may be less returned.
	 * @param offset
	 *            offset
	 * @return objects from db
	 */
	public IObjectsCache getObjects(int maxResultNumber, int offset);
}

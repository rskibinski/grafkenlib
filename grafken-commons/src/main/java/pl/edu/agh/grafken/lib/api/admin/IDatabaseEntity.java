package pl.edu.agh.grafken.lib.api.admin;

import java.sql.Connection;

/**
 * Proxy class responsible for covering database communication layer. Manages
 * connection pool live cycle.
 * 
 */
public interface IDatabaseEntity {

	/**
	 * Creates and configure connection pool for database.
	 * 
	 * @throws ClassNotFoundException
	 */
	public void configureConnectionPool() throws ClassNotFoundException;

	/**
	 * Shutdown connection pool.
	 */
	public void shutdownConnectionPool();

	/**
	 * Returns connection if available.
	 * 
	 * @return {@link Connection} to database.
	 */
	public Connection getConnection();

	/**
	 * Returns database unique name.
	 * 
	 * @return db ID.
	 */
	public String getId();

	/**
	 * Returns driver class String representation
	 * 
	 * @return db driver class
	 */
	public String getDriverClass();
}
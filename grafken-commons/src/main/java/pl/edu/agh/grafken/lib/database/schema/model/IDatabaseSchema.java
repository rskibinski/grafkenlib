package pl.edu.agh.grafken.lib.database.schema.model;

import java.util.Collection;

/**
 * Application representation of database schema. Should provide information
 * for query builder to communicate with {@link DatabaseEntity}
 *
 */
public interface IDatabaseSchema {
	/**
	 * Get database id - as it is declared in application - not in the database
	 * communication layer.
	 * 
	 * @return database id in app
	 */
	public abstract String getDatabaseID();

	/**
	 * Returns schema name.
	 * 
	 * @return schema name
	 */
	public abstract String getSchemaName();

	/**
	 * Returns collection of all tables in db schema.
	 * 
	 * @return database tables.
	 */
	public abstract Collection<IDatabaseTable> getDatabaseTables();

	/**
	 * Add tables to schema.
	 * 
	 * @param table
	 *            Representation of db table in app.
	 */
	public abstract void addTable(IDatabaseTable table);

	/**
	 * Checks if table representation was added to schema and returns it when
	 * positive.
	 * 
	 * @param tableID
	 *            id db table representation
	 * @return db table representation.
	 */
	public abstract IDatabaseTable getTable(String tableID);

	/**
	 * Function wrapper for getting column representation from table
	 * representation
	 * 
	 * @param tableID
	 *            table ID
	 * @param columnID
	 *            column ID
	 * @return representation of db column
	 */
	public abstract IDatabaseColumn getDatabaseColumn(String tableID,
			String columnID);

}
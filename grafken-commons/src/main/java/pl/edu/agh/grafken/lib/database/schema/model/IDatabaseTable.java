package pl.edu.agh.grafken.lib.database.schema.model;

import java.util.Collection;

public interface IDatabaseTable {

	public String getTableName();

	/**
	 * Returns collection of {@link IDatabaseColumn} connected with this
	 * {@link IDatabaseTable}.
	 * 
	 * @return database columns
	 */
	public Collection<IDatabaseColumn> getColumns();

	/**
	 * Returns collection of {@link IPrimaryKeyColumn}
	 * 
	 * @return primary keys
	 */
	public Collection<IPrimaryKeyColumn> getPrimaryKeys();

	/**
	 * Returns collection of {@link IForeignKeyColumn}.
	 * 
	 * @return foreing keys
	 */
	public Collection<IForeignKeyColumn> getForeignKeys();

	/**
	 * Adds {@link IDatabaseColumn} to this {@link IDatabaseTable}.
	 * 
	 * @param column
	 *            db column
	 */

	public void addColumn(IDatabaseColumn column);

	/**
	 * Adds {@link IPrimaryKeyColumn} to this {@link IDatabaseTable}.
	 * 
	 * @param column
	 *            pk column
	 */
	public void addPrimaryKeyColumn(IPrimaryKeyColumn column);

	/**
	 * Adds {@link IForeignKeyColumn} to this {@link IDatabaseTable}.
	 * 
	 * @param foreignKeyColumn
	 *            fk column
	 */
	public void addForeignKeyColumn(IForeignKeyColumn foreignKeyColumn);

	/**
	 * Return column by its' name.
	 * 
	 * @param columnName
	 *            column name
	 * @return Column representation
	 */
	public IDatabaseColumn getColumn(String columnName);

}
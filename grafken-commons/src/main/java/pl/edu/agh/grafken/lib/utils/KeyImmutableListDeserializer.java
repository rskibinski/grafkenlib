package pl.edu.agh.grafken.lib.utils;

import java.io.IOException;
import java.util.Iterator;

import pl.edu.agh.grafken.lib.utils.KeyImmutableList.Builder;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class KeyImmutableListDeserializer extends JsonDeserializer<KeyImmutableList<Object>>{

	@Override
	public KeyImmutableList<Object> deserialize(JsonParser arg0,
			DeserializationContext arg1) throws IOException,
			JsonProcessingException {
		ObjectCodec codec = arg0.getCodec();
		JsonNode node = codec.readTree(arg0);
		Iterator<JsonNode> iterator = node.get("keys").iterator();
		Builder<Object> builder = KeyImmutableList.builder();
		while(iterator.hasNext()){
			String key = iterator.next().textValue();
			builder.add(key);
		}
		return builder.build();
	}

}

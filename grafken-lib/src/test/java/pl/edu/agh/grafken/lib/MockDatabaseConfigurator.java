package pl.edu.agh.grafken.lib;

import java.io.File;

import pl.edu.agh.grafken.lib.database.utils.DatabaseConfigurator;

/**
 * Class with behavior of DatabaseConfigurator for testing purposes.
 * 
 */
public class MockDatabaseConfigurator extends DatabaseConfigurator {

	public MockDatabaseConfigurator(String configurationDirectory) {
		this.configurationDir = configurationDirectory;
		databaseConfigurationPath = new StringBuilder()
				.append(configurationDir).append(File.separator)
				.append("databases.conf").toString();
		mappingDirPath = configurationDir + File.separator + "mapping";
	}

	public MockDatabaseConfigurator(String configurationDirectory,
			String databaseConfigurationFile) {
		this.configurationDir = configurationDirectory;
		databaseConfigurationPath = new StringBuilder()
				.append(configurationDir).append(File.separator)
				.append(databaseConfigurationFile).toString();
		mappingDirPath = configurationDir + File.separator + "mapping";
	}

	public MockDatabaseConfigurator(String configurationDirectory,
			String databaseConfigurationFile, String mappingDirectoryPath) {
		this.configurationDir = configurationDirectory;
		databaseConfigurationPath = new StringBuilder()
				.append(configurationDir).append(File.separator)
				.append(databaseConfigurationFile).toString();
		mappingDirPath = configurationDir + File.separator + "mapping";
	}
}

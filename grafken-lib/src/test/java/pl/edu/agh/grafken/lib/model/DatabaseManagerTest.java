package pl.edu.agh.grafken.lib.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import pl.edu.agh.grafken.lib.MockDatabaseConfigurator;
import pl.edu.agh.grafken.lib.api.admin.IDatabaseConfigurator;
import pl.edu.agh.grafken.lib.database.exceptions.ConfigurationParsingException;
import pl.edu.agh.grafken.lib.database.model.DatabaseManager;

import com.thoughtworks.xstream.converters.ConversionException;

public class DatabaseManagerTest {

	@Test(expected = ConfigurationParsingException.class)
	public void testNoFileConfigurationParsing() throws Exception {
		IDatabaseConfigurator dC = new MockDatabaseConfigurator(
				"/non/existing/path");
		DatabaseManager dbManager = new DatabaseManager(dC);
		dbManager.initializeDatabases();
	}

	@Test
	public void testConfigurationParsing() throws ConfigurationParsingException {
		IDatabaseConfigurator dc = new MockDatabaseConfigurator(
				"conf/test/DatabaseManagerTest");
		DatabaseManager dbManager = new DatabaseManager(dc);
		dbManager.initializeDatabases();
		assertEquals(dbManager.getDatabases().size(), 2);
	}

	@Test(expected = ConversionException.class)
	public void testWrongFileConfigurationParsing()
			throws ConfigurationParsingException {
		IDatabaseConfigurator dc = new MockDatabaseConfigurator(
				"conf/test/DatabaseManagerTest", "test_fail.conf");
		DatabaseManager dbManager = new DatabaseManager(dc);
		dbManager.initializeDatabases();
		assertEquals(dbManager.getDatabases().size(), 2);
	}
}

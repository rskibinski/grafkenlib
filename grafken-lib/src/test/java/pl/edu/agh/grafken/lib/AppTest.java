package pl.edu.agh.grafken.lib;

import org.junit.Test;

import pl.edu.agh.grafken.lib.database.exceptions.ConfigurationParsingException;
import pl.edu.agh.grafken.lib.utils.KeyImmutableList;

public class AppTest extends TestBase{

	public GrafkenDaemon app;

	@Override
	public void setUp() throws Exception {
		super.setUp();
		app = (GrafkenDaemon) GrafkenDaemon.getAdminInstance();
	}

	@Test(expected = ConfigurationParsingException.class)
	public void testNoConfigurationFileTest() {
		try {
			app.setDatabaseConfigurator(new MockDatabaseConfigurator(
					"non/existing/path.conf"));
			app.init();
		} catch (ExitException e) {
			assertEquals("Exit status", 1, e.status);
		}
	}
	
	@Test
	public void testApplicationInit(){
		app.setDatabaseConfigurator(new MockDatabaseConfigurator(
				"conf/test"));
		app.init();
		assertEquals("Configured databases quantity", app.getDatabasesManager().getDatabases().size(), 1);
	}
	
	@Test
	public void testKIL()
	{
		
		pl.edu.agh.grafken.lib.utils.KeyImmutableList.Builder<String> builder = KeyImmutableList.builder();
		pl.edu.agh.grafken.lib.utils.KeyImmutableList.Builder<String> builder2 = KeyImmutableList.builder();
		
		builder.add("A");
		builder2.add("ASD");
		builder2.add("A");
		builder.add("ASD");
		KeyImmutableList<String> build = builder.build();
		KeyImmutableList<String> build2 = builder2.build();
		System.out.println(build.hashCode());
		System.out.println(build2.hashCode());
		System.out.println(build.equals(build2));
	}

}

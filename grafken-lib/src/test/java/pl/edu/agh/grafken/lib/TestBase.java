package pl.edu.agh.grafken.lib;

import java.security.Permission;

import junit.framework.TestCase;

public abstract class TestBase extends TestCase {

	@SuppressWarnings("serial")
	protected static class ExitException extends SecurityException {
		public final int status;
	
		public ExitException(int status) {
			this.status = status;
		}
	}

	protected static class NoExitSecurityManager extends SecurityManager {
	
			@Override
			public void checkExit(int status) {
				super.checkExit(status);
				throw new ExitException(status);
			}
	
			@Override
			public void checkPermission(Permission perm, Object context) {
			}
	
			@Override
			public void checkPermission(Permission perm) {
			}
	
		}

	public TestBase() {
		super();
	}

	public TestBase(String name) {
		super(name);
	}

	@Override
	public void setUp() throws Exception {
		super.setUp();
		System.setSecurityManager(new NoExitSecurityManager());
	}

	@Override
	public void tearDown() throws Exception {
		System.setSecurityManager(null);
		super.tearDown();
	}

}
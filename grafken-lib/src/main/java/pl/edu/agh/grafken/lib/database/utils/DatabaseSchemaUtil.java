package pl.edu.agh.grafken.lib.database.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import pl.edu.agh.grafken.lib.api.admin.IDatabaseEntity;
import pl.edu.agh.grafken.lib.utils.StringUtils;


public class DatabaseSchemaUtil {

	private static final HashMap<String, String> DRIVERTYPE_TABLES_QUERY = new HashMap<String, String>();
	private static final HashMap<String, String> DRIVERTYPE_TABLE_NAME = new HashMap<String, String>();

	/**
	 * Returns result set containing all tables in given database.
	 * 
	 * @param db
	 *            database
	 * @return tables result set
	 * @throws SQLException
	 */
	public static ResultSet getTablesResultSet(IDatabaseEntity db)
			throws SQLException {
		Connection connection = db.getConnection();
		String tablesQuery = DRIVERTYPE_TABLES_QUERY.get(db.getDriverClass());

		ResultSet tables = connection.getMetaData().getTables(null, null, "%",
				new String[] { tablesQuery });
		connection.close();
		return tables;

	}

	/**
	 * Returns names of the tables from tables result set.
	 * 
	 * @param db
	 *            database
	 * @return Array list of table names in database.
	 */
	public static ArrayList<String> getTableNames(IDatabaseEntity db) {
		try {
			ResultSet tables = getTablesResultSet(db);
			ArrayList<String> result = new ArrayList<String>();

			while (tables.next()) {
				result.add(tables.getString(DRIVERTYPE_TABLE_NAME.get(db
						.getDriverClass())));
			}

			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Returns result set containing all column on given table name.
	 * 
	 * @param db
	 *            database
	 * @param tableName
	 *            table Name
	 * @return columns result set
	 * @throws SQLException
	 */
	public static ResultSet getTableColumnsResultSet(IDatabaseEntity db,
			String tableName) {
		Connection connection = db.getConnection();
		try {
			PreparedStatement statement = connection
					.prepareStatement("SELECT * FROM " + StringUtils.quoteArgument(tableName));
			ResultSet tableInfo = statement.executeQuery();
			return tableInfo;
		} catch (SQLException e) {
			e.printStackTrace();
			return null; // TODO: Exception throw?
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Returns list of table column names.
	 * 
	 * @param db
	 *            database
	 * @param tableName
	 *            table name
	 * @return list of table column names
	 */
	public static ArrayList<String> getTableColumnNames(IDatabaseEntity db,
			String tableName) {
		ResultSet resultSet = getTableColumnsResultSet(db, tableName);
		ArrayList<String> columnNames = new ArrayList<String>();

		try {
			for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
				columnNames.add(resultSet.getMetaData().getColumnName(i));
			}
			return columnNames;
		} catch (SQLException e) {
			e.printStackTrace();
			return null; // TODO: Exception throw?
		}
	}

	/**
	 * Return primary keys result set for table.
	 * 
	 * @param db
	 *            database
	 * @param tableName
	 *            table name
	 * @return primary keys result set for table
	 */
	public static ResultSet getPrimaryKeysResutlSet(IDatabaseEntity db,
			String tableName) {
		Connection connection = db.getConnection();

		try {
			ResultSet primaryKeys = connection.getMetaData().getPrimaryKeys(
					null, null, tableName);
			return primaryKeys;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Returns list of primary keys names.
	 * 
	 * @param db
	 *            database
	 * @param tableName
	 *            table name
	 * @return list of pks
	 */
	public static ArrayList<String> getTablePrimaryKeys(IDatabaseEntity db,
			String tableName) {
		ResultSet pks = getPrimaryKeysResutlSet(db, tableName);
		ArrayList<String> pksList = new ArrayList<String>();

		try {
			while (pks.next()) {
				pksList.add(pks.getString(4));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return pksList;
	}

	public static ResultSet getForeignKeysResultSet(IDatabaseEntity db,
			String tableName) {
		Connection connection = db.getConnection();

		try {
			ResultSet importedKeys = connection.getMetaData().getImportedKeys(
					null, null, tableName);
			return importedKeys;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	/**
	 * 
	 * @param db
	 * @param tableName
	 * @return
	 */
	public static ArrayList<String[]> getTableForeignKeys(
			IDatabaseEntity db, String tableName) {
		ResultSet resultSet = getForeignKeysResultSet(db, tableName);
		ArrayList<String[]> list = new ArrayList<>();
		try {
			while (resultSet.next()) {
				String fk_details[] = new String[3];
				fk_details[0] = resultSet.getString(8);
				fk_details[1] = resultSet.getString(3);
				fk_details[2] = resultSet.getString(4);
				list.add(fk_details);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return list;
	}

	public static void testMethod(IDatabaseEntity db, String tableName) {
		Connection connection = db.getConnection();

		try {
			ResultSet pks = connection.getMetaData().getPrimaryKeys(null, null,
					tableName);
			while (pks.next()) {
				System.out.println("PK : " + tableName + "  "
						+ pks.getString(4));
			}
			ResultSet fks = connection.getMetaData().getImportedKeys(null,
					null, tableName);
			while (fks.next()) {
				System.out.println("FK : " + " " + fks.getString(8)
						+ " referenced to table: " + fks.getString(3) + " : "
						+ fks.getString(4));
			}
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	static {
		DRIVERTYPE_TABLES_QUERY.put("org.postgresql.Driver", "TABLE");
		DRIVERTYPE_TABLES_QUERY.put("org.sqlite.JDBC", "TABLE");
		
		DRIVERTYPE_TABLE_NAME.put("org.postgresql.Driver", "TABLE_NAME");
		DRIVERTYPE_TABLE_NAME.put("org.sqlite.JDBC", "TABLE_NAME");
	}

}

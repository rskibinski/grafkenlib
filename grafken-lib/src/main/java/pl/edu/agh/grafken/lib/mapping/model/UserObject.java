package pl.edu.agh.grafken.lib.mapping.model;

import java.util.ArrayList;

import pl.edu.agh.grafken.lib.api.user.IObjectMetaInformation;
import pl.edu.agh.grafken.lib.api.user.IUserObject;
import pl.edu.agh.grafken.lib.utils.KeyImmutableList;
import pl.edu.agh.grafken.lib.wrapper.interfaces.IObjectMetaInformationWrapper;

public class UserObject implements IUserObject {

	public static class Builder {
		private KeyImmutableList.Builder<String> uidBuilder = KeyImmutableList
				.builder();

		private IObjectMetaInformation metaInformation;

		private ArrayList<String> propertiesValues = new ArrayList<>(); // TODO: Types
		private ArrayList<String> forwardBindings = new ArrayList<>();

		public Builder(IObjectMetaInformation meta) {
			this.metaInformation = meta;
		}

		public Builder addValue(String value) { // TODO Types.
			propertiesValues.add(value);
			return this;
		}

		public Builder addForwardBinding(String binding) {
			forwardBindings.add(binding);
			return this;
		}

		public Builder addUidValue(String uidValue) {
			uidBuilder.add(uidValue);
			return this;
		}

		public UserObject build() {
			return new UserObject(this);
		}
	}

	private KeyImmutableList<String> uid;

	private IObjectMetaInformation metaInformation;

	private ArrayList<String> propertiesValues; // TODO
												// Tyypes!
	private ArrayList<String> forwardBindings;

	private UserObject(Builder b) {
		this.metaInformation = b.metaInformation;
		this.propertiesValues = b.propertiesValues;
		this.forwardBindings = b.forwardBindings;
		this.uid = b.uidBuilder.build();
	}

	@Override
	public KeyImmutableList<String> getKey() {
		return uid;
	}

	@Override
	public String getPropertyValue(int index) {
		return propertiesValues.get(index);
	}

	@Override
	public ArrayList<String> getValuesList() {
		return propertiesValues;
	}

	@Override
	public ArrayList<String> getForwardBindings() {
		return forwardBindings;
	}

	@Override
	public IObjectMetaInformation getMetaInformation() {
		return this.metaInformation;
	}

}
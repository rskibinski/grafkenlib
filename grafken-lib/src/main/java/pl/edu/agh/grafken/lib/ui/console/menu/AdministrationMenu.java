package pl.edu.agh.grafken.lib.ui.console.menu;

import java.util.Scanner;

import pl.edu.agh.grafken.lib.ui.console.commands.MenuCommand;

public class AdministrationMenu extends AbstractMenu implements IMenu {

	public AdministrationMenu(Scanner scanner) {
		super(scanner);
		entries.put(1, new MenuEntry("Databases configuration", 1, new MenuCommand(new DatabaseConfigurationMenu(scanner))));
		entries.put(2, new MenuEntry("Mapping configuration", 2, new MenuCommand(new ChooseDatabaseMenu(scanner))));
	}

}

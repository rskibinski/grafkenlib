package pl.edu.agh.grafken.lib.database.model;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import pl.edu.agh.grafken.lib.api.admin.IDatabaseEntity;

import com.jolbox.bonecp.BoneCP;
import com.jolbox.bonecp.BoneCPConfig;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@XStreamAlias("database")
public class DatabaseEntity implements IDatabaseEntity {

	static final Logger log = Logger.getLogger(DatabaseEntity.class);

	private String name;
	
	@XStreamOmitField
	private BoneCP connectionPool;

	private String driverClass;

	private String jdbcUrl;

	private String username;

	private String password;

	private static final int MIN_CONNECTIONS_PER_PARTITION = 5;

	private static final int MAX_CONNECTIONS_PER_PARTITION = 20;

	private static final int NUMBER_OF_PARTITIONS = 1;

	/**
	 * Constructor for DatabaseEntity.
	 * 
	 * @param name
	 *            name under DatabaseEntity will be accesible in application
	 * @param driverClass
	 *            database driver class
	 * @param jdbcUrl
	 *            address under db is accessible
	 * @param username
	 *            username credential to db
	 * @param password
	 *            password credential to db
	 */
	public DatabaseEntity(String name, String driverClass, String jdbcUrl,
			String username, String password) {
		super();
		this.setName(name);
		this.driverClass = driverClass;
		this.jdbcUrl = jdbcUrl;
		this.username = username;
		this.password = password;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see database.model.IDatabaseEntity#configureConnectionPool()
	 */
	@Override
	public void configureConnectionPool() throws ClassNotFoundException {
		Class.forName(driverClass);

		BoneCPConfig config = new BoneCPConfig();
		config.setJdbcUrl(jdbcUrl);
		config.setUsername(username);
		config.setPassword(password);
		config.setLazyInit(false);

		config.setMinConnectionsPerPartition(MIN_CONNECTIONS_PER_PARTITION);
		config.setMaxConnectionsPerPartition(MAX_CONNECTIONS_PER_PARTITION);
		config.setPartitionCount(NUMBER_OF_PARTITIONS);

		try {
			connectionPool = new BoneCP(config);
			 log.info("Connection pool to database" + getJdbcUrl()
			 + " was established on database name:" + name);
		} catch (SQLException e) {
			 log.error("Error while aquiring connection pool for database: "
			 + jdbcUrl);
			e.printStackTrace();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see database.model.IDatabaseEntity#shutdownConnectionPool()
	 */
	@Override
	public void shutdownConnectionPool() {
		if (connectionPool != null) {
			connectionPool.shutdown();
			 log.info("Connection pool for " + getJdbcUrl() +
			 " shut down!");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see database.model.IDatabaseEntity#getConnection()
	 */
	@Override
	public Connection getConnection() {
		Connection conn = null;
		try {
			conn = connectionPool.getConnection();
		} catch (SQLException e) {
			 log.error("Error while aquiring connection from " +
			 getJdbcUrl());
			e.printStackTrace();
		}
		return conn;
	}

	// BEGIN Setters&getters
	@Override
	public String getDriverClass() {
		return driverClass;
	}

	public void setDriverClass(String driverClass) {
		this.driverClass = driverClass;
	}

	public String getJdbcUrl() {
		return jdbcUrl;
	}

	public void setJdbcUrl(String jdbcUrl) {
		this.jdbcUrl = jdbcUrl;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String getId() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	// END Setters&getters

	@Override
	public String toString() {
		return this.getId();
	}
}

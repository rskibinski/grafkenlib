package pl.edu.agh.grafken.lib.database.model;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import pl.edu.agh.grafken.lib.api.admin.IDatabaseConfigurator;
import pl.edu.agh.grafken.lib.api.admin.IDatabaseEntity;
import pl.edu.agh.grafken.lib.api.admin.IDatabaseManager;
import pl.edu.agh.grafken.lib.database.exceptions.ConfigurationParsingException;
import pl.edu.agh.grafken.lib.database.exceptions.NotSuchDatabaseInitializedException;
import pl.edu.agh.grafken.lib.database.schema.utils.SchemaSerializer;
import pl.edu.agh.grafken.lib.utils.StringUtils;

/**
 * Class responsible for managing initialized DatabaseEntity objects in
 * application.
 * 
 */
public class DatabaseManager implements IDatabaseManager {

	static final Logger LOG = Logger.getLogger(DatabaseManager.class);

	private final HashMap<String, IDatabaseEntity> DATABASES_MAP = new HashMap<String, IDatabaseEntity>();

	private IDatabaseConfigurator databaseConfigurator;

	public DatabaseManager(IDatabaseConfigurator databaseConfigurator) {
		setDatabaseConfigurator(databaseConfigurator);
	}

	public void setDatabaseConfigurator(
			IDatabaseConfigurator databaseConfigurator) {
		this.databaseConfigurator = databaseConfigurator;
	}

	/**
	 * Parse configuration file and initialize connection pool for each
	 * correctly defined database.
	 * 
	 * @param configurationPath
	 * @throws ConfigurationParsingException
	 */
	@Override
	public void initializeDatabases() throws ConfigurationParsingException {
		LOG.info("Starting database initialization");
		ArrayList<? extends IDatabaseEntity> databases = databaseConfigurator
				.parseFileForDatabaseConfigurations();
		for (IDatabaseEntity db : databases) {
			try {
				db.configureConnectionPool();
				DATABASES_MAP.put(db.getId(), db);
			} catch (ClassNotFoundException e) {
				LOG.error("Unable to aquire jdbc driver class for database "
						+ StringUtils.quoteArgument(db.getId()));
			}
		}
	}

	/**
	 * Shutting down databases connections pools.
	 */
	public void shutdownDatabases() {
		LOG.info("Shutting down databases connections pools");
		for (IDatabaseEntity db : DATABASES_MAP.values()) {
			db.shutdownConnectionPool();
		}
		LOG.info("Shutting down finished");
	}
	
	public void reset() throws ConfigurationParsingException{
		shutdownDatabases();
		initializeDatabases();
	}

	/**
	 * Gets all DatabaseEntity objects initialized.
	 * 
	 * @return list of DatabasEntity objects
	 */
	@Override
	public ArrayList<IDatabaseEntity> getDatabases() {
		return new ArrayList<IDatabaseEntity>(DATABASES_MAP.values());
	}

	/**
	 * Check if DatabaseEntity is found under name and returns it if any.
	 * Otherwise throws accurate exception.
	 * 
	 * @param name
	 *            database name as registered in the application - NOT as
	 *            database address/file name.
	 * @return DatabaseEntity found under name
	 * @throws NotSuchDatabaseInitializedException
	 */
	@Override
	public IDatabaseEntity getDatabase(String name)
			throws NotSuchDatabaseInitializedException {
		IDatabaseEntity db = DATABASES_MAP.get(name);
		if (db != null) {
			return db;
		} else {
			throw new NotSuchDatabaseInitializedException();
		}
	}

	@Override
	public String getDefaultMappingFilePathForDatabase(String databaseName) {
		return databaseConfigurator.getDefaultMappingFilePath(databaseName);
	}

	@Override
	public IDatabaseConfigurator getConfigurator() {
		return databaseConfigurator;
	}

	@Override
	public String getEmptyMappingFileForDatabase(String databaseName)
			throws NotSuchDatabaseInitializedException {
		return SchemaSerializer.marshallToCustomForm(getDatabase(databaseName));
	}
}

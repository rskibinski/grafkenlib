package pl.edu.agh.grafken.lib.mapping.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import pl.edu.agh.grafken.lib.GrafkenDaemon;
import pl.edu.agh.grafken.lib.api.admin.IDatabaseEntity;
import pl.edu.agh.grafken.lib.api.user.IObjectMetaInformation;
import pl.edu.agh.grafken.lib.api.user.IObjectMetaReference;
import pl.edu.agh.grafken.lib.api.user.IObjectsCache;
import pl.edu.agh.grafken.lib.api.user.IUserObject;
import pl.edu.agh.grafken.lib.database.exceptions.NotSuchDatabaseInitializedException;
import pl.edu.agh.grafken.lib.database.schema.model.IDatabaseColumn;
import pl.edu.agh.grafken.lib.mapping.model.ObjectsCache;
import pl.edu.agh.grafken.lib.mapping.model.UserObject;
import pl.edu.agh.grafken.lib.utils.StringUtils;

public class QueryBuilder implements IQueryBuilder {

	private static QueryBuilder instance;

	private QueryBuilder() {
	}

	public static QueryBuilder getInstance() {
		if (instance == null) {
			instance = new QueryBuilder();
		}
		return instance;
	}

	private static Logger log = Logger.getLogger(QueryBuilder.class);

	@Override
	// TODO: Merge it with methods below
	public IObjectsCache getObjectData(IObjectMetaInformation metaData,
			String databaseName) {
		IObjectsCache objectCollection = null;
		try {
			IDatabaseEntity database = GrafkenDaemon.getAdminInstance()
					.getDatabasesManager().getDatabase(databaseName);

			String query = getQuery(metaData);
			objectCollection = queryDatabase(query, database, new ArrayList<>(
					metaData.getFieldsNames()), metaData);

		} catch (NotSuchDatabaseInitializedException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return objectCollection;
	}

	public IObjectsCache getObjectData(IObjectMetaInformation metaData,
			String databaseName, int limit) {
		return getObjectData(metaData, databaseName, limit, 0);
	}

	public IObjectsCache getObjectData(IObjectMetaInformation metaData,
			String databaseName, int limit, int offset) {
		IObjectsCache objectCollection = null;
		try {
			IDatabaseEntity database = GrafkenDaemon.getAdminInstance()
					.getDatabasesManager().getDatabase(databaseName);

			String query = limitQuery(getQuery(metaData), limit, offset);
			objectCollection = queryDatabase(query, database, new ArrayList<>(
					metaData.getFieldsNames()), metaData);

		} catch (NotSuchDatabaseInitializedException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return objectCollection;
	}

	@Override
	public IObjectsCache getObjectsConnected(
			IObjectMetaReference ref, String databaseName,
			String information) {
		String query = getForeignKeyQuery(ref, information);
		IObjectsCache objectsConnected = null;
		try {
			IDatabaseEntity database = GrafkenDaemon.getAdminInstance()
					.getDatabasesManager().getDatabase(databaseName);
			objectsConnected = queryDatabase(query, database, new ArrayList<>(
					ref.getForwardReferenceMeta().getFieldsNames()),
					ref.getForwardReferenceMeta());

		} catch (NotSuchDatabaseInitializedException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return objectsConnected;
	}

	@Override
	public IObjectsCache searchForObject(IObjectMetaInformation meta,
			String value) throws NotSuchDatabaseInitializedException {
		String query = getQuery(meta);
		StringBuilder b = new StringBuilder(query.substring(0,
				query.length() - 1));
		b.append(" WHERE ");
		b.append(getSearchAtom(meta.getFieldsMapping(), value, "LIKE", "OR"));
		b.append(";");
		query = b.toString();
		IObjectsCache results = null;
		IDatabaseEntity db = GrafkenDaemon.getAdminInstance()
				.getDatabasesManager()
				.getDatabase(meta.getParent().getDatabaseID());
		try {
			results = queryDatabase(query, db,
					new ArrayList<>(meta.getFieldsNames()), meta);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return results;
	}

	private String getSearchAtom(
			Map<String, Collection<IDatabaseColumn>> fields, String value,
			String operator, String logicJunction) {
		Set<String> columnsSet = new HashSet<>();
		for (Collection<IDatabaseColumn> columns : fields.values()) {
			for (IDatabaseColumn column : columns) {
				columnsSet.add(new StringBuilder()
						.append(column.getTableName()).append(".")
						.append(column.getColumnName()).toString());
			}
		}
		StringBuilder b = new StringBuilder();
		for (String column : columnsSet) {
			b.append(column).append(" ").append(operator).append(" ")
					.append(StringUtils.singleQuoteArgument(value)).append(" ")
					.append(logicJunction).append(" ");
		}
		b.delete(b.length() - logicJunction.length() -1, b.length());
		return b.toString().trim();
	}

	private IObjectsCache queryDatabase(String query, IDatabaseEntity database,
			ArrayList<String> fieldNames, IObjectMetaInformation metaData)
			throws SQLException {
		log.debug(query); 
		IObjectsCache objectCollection;
		// split
		Connection connection = database.getConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(query);
		UserObject.Builder object;
		ArrayList<UserObject.Builder> buildersList = new ArrayList<>();
		while (rs.next()) {
			object = new UserObject.Builder(metaData);
			for (String fieldName : metaData.getFieldsNames()) {
				object.addValue(rs.getString(fieldName));
			}
			ArrayList<String> uidNames = new ArrayList<>();
			for (IDatabaseColumn uidColumn : metaData.getUidColumns()) {
				uidNames.add(uidColumn.getColumnName());
			}
			for (String uidColumn : uidNames) {
				object.addUidValue(rs.getString(uidColumn));
			}
			for (IObjectMetaReference ref : metaData
					.getConnectedObjectsMetaInformation()) {
				String bindData = rs.getString(ref.getOriginColumnName());
				object.addForwardBinding(bindData);
			}
			buildersList.add(object);
		}
		connection.close();

		ArrayList<IUserObject> objects = new ArrayList<IUserObject>();
		for (UserObject.Builder b : buildersList) {
			objects.add(b.build());
		}
		objectCollection = new ObjectsCache(metaData, objects);
		return objectCollection;
	}

	private String getQuery(IObjectMetaInformation meta) {
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT ");

		Set<String> tableNames = new HashSet<>();
		Set<String> columnNames = new HashSet<>();
		for (Collection<IDatabaseColumn> fieldColumns : meta.getFieldsMapping()
				.values()) {
			for (IDatabaseColumn column : fieldColumns) {
				columnNames.add(new StringBuffer()
						.append(column.getTableName()).append(".")
						.append(column.getColumnName()).toString());
				tableNames.add(column.getTableName());
			}
		}

		for (IDatabaseColumn column : meta.getUidColumns()) {
			columnNames.add(new StringBuffer().append(column.getTableName())
					.append(".").append(column.getColumnName()).toString());
			tableNames.add(column.getTableName());
		}

		for (IObjectMetaReference reference : meta
				.getConnectedObjectsMetaInformation()) {// binding columns
			columnNames.add(new StringBuffer()
					.append(reference.getOriginTableName()).append(".")
					.append(reference.getOriginColumnName()).toString());
			tableNames.add(reference.getOriginTableName());
		}
		for (String columName : columnNames) {
			sqlBuilder.append(columName).append(",");
		}

		sqlBuilder.deleteCharAt(sqlBuilder.length() - 1); // deletes last comma
		sqlBuilder.append(" FROM ");

		for (String tableName : tableNames) {
			sqlBuilder.append(tableName);
			sqlBuilder.append(",");
		}
		sqlBuilder.deleteCharAt(sqlBuilder.length() - 1); // deletes last comma
		sqlBuilder.append(";");
		log.info(sqlBuilder.toString());
		return sqlBuilder.toString();
	}

	private String limitQuery(String originalQuery, int limit, int offset) {
		if (!originalQuery.isEmpty()) {
			String trim = originalQuery.trim();
			trim = trim.substring(0, trim.length() - 1).concat(" LIMIT ")
					.concat(Integer.toString(limit)).concat(" OFFSET ")
					.concat(Integer.toString(offset)).concat(";");
			return trim;
		} else {
			return "";
		}
	}

	private String getForeignKeyQuery(IObjectMetaInformation meta,
			List<Pair<String, String>> databaseColumns,
			Pair<String, String> primaryKeyBinding) { 
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT ");

		Set<String> databaseTables = new HashSet<>();
		Set<String> databaseColumnsWithTables = new HashSet<>();
		for (Pair<String, String> pair : databaseColumns) {// data columns
			databaseColumnsWithTables.add(new StringBuffer()
					.append(pair.getLeft()).append(".").append(pair.getRight())
					.toString());
			databaseTables.add(pair.getLeft());
		}
		for (IObjectMetaReference reference : meta
				.getConnectedObjectsMetaInformation()) {// binding columns
			databaseColumnsWithTables.add(new StringBuffer()
					.append(reference.getOriginTableName()).append(".")
					.append(reference.getOriginColumnName()).toString());
			databaseTables.add(reference.getOriginTableName());
		}
		for (IDatabaseColumn column : meta.getUidColumns()) {
			databaseColumnsWithTables.add(new StringBuffer()
					.append(column.getTableName()).append(".")
					.append(column.getColumnName()).toString());
			databaseTables.add(column.getTableName());
		}

		for (String column : databaseColumnsWithTables) {
			sqlBuilder.append(column).append(",");
		}
		sqlBuilder.deleteCharAt(sqlBuilder.length() - 1);

		sqlBuilder.append(" FROM ");
		for (String table : databaseTables) {
			sqlBuilder.append(table).append(",");
		}
		sqlBuilder.deleteCharAt(sqlBuilder.length() - 1);

		sqlBuilder.append(" WHERE ");
		sqlBuilder
				.append(primaryKeyBinding.getLeft()
						+ "="
						+ StringUtils.singleQuoteArgument(primaryKeyBinding
								.getRight()));
		sqlBuilder.append(";");

		return sqlBuilder.toString();
	}

	private String getForeignKeyQuery(IObjectMetaReference reference,
			String information) {
		List<Pair<String, String>> backwardReference = dereferenceFields(reference
				.getForwardReferenceMeta());

		String left = reference.getForeignTableName() + "."
				+ reference.getForeignColumnName();
		String query = getForeignKeyQuery(reference.getForwardReferenceMeta(),
				backwardReference, Pair.of(left, information));
		log.debug(query);
		return query;
	}

	private List<Pair<String, String>> dereferenceFields(
			IObjectMetaInformation object) {
		List<Pair<String, String>> list = new ArrayList<>();
		Collection<Collection<IDatabaseColumn>> values = object.getFieldsMapping()
				.values();
		for (Collection<IDatabaseColumn> columns : values) {
			for (IDatabaseColumn column : columns) {
				list.add(Pair.of(column.getTableName(), column.getColumnName()));
			}
		}

		return list;
	}
}

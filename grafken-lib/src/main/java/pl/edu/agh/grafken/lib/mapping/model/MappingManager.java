package pl.edu.agh.grafken.lib.mapping.model;

import java.util.HashMap;
import java.util.Map;

import pl.edu.agh.grafken.lib.api.admin.IMappingManager;
import pl.edu.agh.grafken.lib.api.user.IMapping;

public class MappingManager implements IMappingManager {
	private Map<String, IMapping> MAPPING_MAP = new HashMap<>();

	/* (non-Javadoc)
	 * @see pl.edu.agh.grafken.lib.mapping.model.IMappingManager#addMapping(pl.edu.agh.grafken.lib.mapping.model.Mapping)
	 */
	@Override
	public void addMapping(IMapping mm) {
		MAPPING_MAP.put(mm.getDatabaseID(), mm);
	}

	/* (non-Javadoc)
	 * @see pl.edu.agh.grafken.lib.mapping.model.IMappingManager#getMappingForDatabase(java.lang.String)
	 */
	@Override
	public IMapping getMappingForDatabase(String databaseName) {
		return MAPPING_MAP.get(databaseName);
	}
	
	@Override
	public void reset(){
		MAPPING_MAP = new HashMap<>();
	}

}

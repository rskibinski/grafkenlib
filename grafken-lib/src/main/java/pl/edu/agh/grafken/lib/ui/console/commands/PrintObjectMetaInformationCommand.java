package pl.edu.agh.grafken.lib.ui.console.commands;

import pl.edu.agh.grafken.lib.api.user.IObjectMetaReference;
import pl.edu.agh.grafken.lib.api.user.IObjectMetaInformation;

public class PrintObjectMetaInformationCommand implements ICommand {

	private IObjectMetaInformation meta;
	private boolean ifPrintWithDetails;

	public PrintObjectMetaInformationCommand(IObjectMetaInformation meta,
			boolean ifPrintWithDetails) {
		this.meta = meta;
		this.ifPrintWithDetails = ifPrintWithDetails;
	}

	@Override
	public void execute() {
		if (ifPrintWithDetails) {
			printWithDetails();
		} else
			printWithoutDetails();
	}

	private void printWithDetails() {
		System.out.println("\t" + meta.getName());
		for (String fieldName : meta.getFieldsNames()) {
			System.out.println("\t\t" + fieldName);
		}
		if (meta.getConnectedObjectsMetaInformation().size() > 0) {
			System.out.println("\t\tConnected objects:");
			for (IObjectMetaReference connectedObjectMeta : meta
					.getConnectedObjectsMetaInformation()) {
				System.out.println("\t\t"
						+ connectedObjectMeta.getForwardReferenceMeta().getName());
			}
		}
	}

	private void printWithoutDetails() {
		System.out.println("\t" + meta.getName());
	}
}

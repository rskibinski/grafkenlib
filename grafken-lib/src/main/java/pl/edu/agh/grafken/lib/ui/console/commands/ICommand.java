package pl.edu.agh.grafken.lib.ui.console.commands;

public interface ICommand {

	public void execute();

}
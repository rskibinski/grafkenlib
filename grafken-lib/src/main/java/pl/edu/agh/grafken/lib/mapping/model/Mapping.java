package pl.edu.agh.grafken.lib.mapping.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import pl.edu.agh.grafken.lib.api.user.IMapping;
import pl.edu.agh.grafken.lib.api.user.IObjectMetaInformation;
import pl.edu.agh.grafken.lib.mapping.utils.SearchQuery;

public class Mapping implements IMapping {

	private String databaseID;

	private Map<String, IObjectMetaInformation> objects = new HashMap<String, IObjectMetaInformation>();

	public Mapping(String databaseID) {
		this.databaseID = databaseID;
	}

	public void addUserObject(IObjectMetaInformation object) {
		objects.put(object.getName(), object);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pl.edu.agh.grafken.lib.mapping.model.IMapping#getObjects()
	 */
	@Override
	public Map<String, IObjectMetaInformation> getObjectsMeta() {
		return objects;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pl.edu.agh.grafken.lib.mapping.model.IMapping#getObjectMetaInformation
	 * (java.lang.String)
	 */
	@Override
	public IObjectMetaInformation getObjectMetaInformation(String objectName) {
		return objects.get(objectName);
	}

	public List<IObjectMetaInformation> searchForObjects(String objectName,
			SearchQuery query) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pl.edu.agh.grafken.lib.mapping.model.IMapping#getUserObjectNames()
	 */
	@Override
	public Set<String> getUserObjectNames() {
		return objects.keySet();
	}

	@Override
	public String getDatabaseID() {
		return databaseID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pl.edu.agh.grafken.lib.mapping.model.IMapping#isValid()
	 */
	@Override
	public boolean isValid() {
		return true;
		// TODO: Stub.
	}

}

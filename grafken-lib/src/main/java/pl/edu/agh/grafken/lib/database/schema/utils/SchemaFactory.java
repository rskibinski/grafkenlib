package pl.edu.agh.grafken.lib.database.schema.utils;

import java.util.ArrayList;

import pl.edu.agh.grafken.lib.api.admin.IDatabaseEntity;
import pl.edu.agh.grafken.lib.database.schema.model.AbstractDatabaseColumn;
import pl.edu.agh.grafken.lib.database.schema.model.DatabaseColumn;
import pl.edu.agh.grafken.lib.database.schema.model.DatabaseSchema;
import pl.edu.agh.grafken.lib.database.schema.model.DatabaseTable;
import pl.edu.agh.grafken.lib.database.schema.model.ForeignKeyColumn;
import pl.edu.agh.grafken.lib.database.schema.model.IDatabaseSchema;
import pl.edu.agh.grafken.lib.database.schema.model.IDatabaseTable;
import pl.edu.agh.grafken.lib.database.schema.model.PrimaryKeyColumn;
import pl.edu.agh.grafken.lib.database.utils.DatabaseSchemaUtil;

/**
 * Class creating database schema for purpose of its relation representation using existing {@link IDatabaseEntity}
 * @author testuser
 *
 */
public class SchemaFactory {
	/**
	 * Given {@link IDatabaseEntity} create {@link IDatabaseSchema} with
	 * distinction to primary and foreign keys.
	 * 
	 * @param db
	 *            database
	 * @return {@link IDatabaseSchema} with primary and foreign keys
	 *         distinguished.
	 */
	public static DatabaseSchema createSchema(IDatabaseEntity db) {	
		DatabaseSchema databaseSchema = new DatabaseSchema(db.getId());

		ArrayList<String> tableNames = DatabaseSchemaUtil.getTableNames(db);

		for (String tableName : tableNames) {
			IDatabaseTable databaseTable = new DatabaseTable(tableName);

			ArrayList<String> tablePrimaryKeys = DatabaseSchemaUtil
					.getTablePrimaryKeys(db, tableName);
			for (String columnName : tablePrimaryKeys) {
				databaseTable.addPrimaryKeyColumn(new PrimaryKeyColumn(
						columnName, databaseTable));
			}

			ArrayList<String> tableColumnNames = DatabaseSchemaUtil
					.getTableColumnNames(db, tableName);
			for (String columnName : tableColumnNames) {
				AbstractDatabaseColumn databaseColumn = new DatabaseColumn(
						columnName, databaseTable);
				databaseTable.addColumn(databaseColumn);
			}

			ArrayList<String[]> tableForeignKeys = DatabaseSchemaUtil
					.getTableForeignKeys(db, tableName);
			for (String[] columnName : tableForeignKeys) {
				databaseTable.addForeignKeyColumn(new ForeignKeyColumn(
						columnName[0], columnName[1], columnName[2],
						databaseTable));
			}

			databaseSchema.addTable(databaseTable);
		}
		return databaseSchema;
	}
}

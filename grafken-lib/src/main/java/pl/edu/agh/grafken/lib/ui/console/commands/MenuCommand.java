package pl.edu.agh.grafken.lib.ui.console.commands;

import pl.edu.agh.grafken.lib.ui.console.menu.IMenu;

public class MenuCommand extends AbstractCommand {
	private IMenu menu;

	public MenuCommand(IMenu menu) {
		this.menu = menu;
	}

	@Override
	public void execute() {
		menu.takeControl(menu.getApplicationContext());
	}

}

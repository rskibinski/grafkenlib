package pl.edu.agh.grafken.lib.database.schema.model;

import java.sql.Types;
import java.util.HashMap;

public class ColumnValueType {
	
	private static final String STRING_NAME = "java.lang.String";
	private static final String BYTE_ARRAY_NAME = "java.lang.Byte[]";
	private static final String BOOLEAN_NAME = null;

	private final static HashMap<Integer, String> MAP = new HashMap<>();
	private static final String SHORT_NAME = null;
	private static final String INTEGER_NAME = null;
	private static final String DOUBLE_NAME = null;
	private static final String FLOAT_NAME = null;
	private static final String LONG_NAME = null;
	private static final String MATH_BIGDECIMAL = null;
	private static final String DATE_NAME = null;
	
	static{
		MAP.put(Types.CHAR, STRING_NAME);
		MAP.put(Types.VARCHAR, STRING_NAME);
		MAP.put(Types.LONGVARCHAR, STRING_NAME);
		
		MAP.put(Types.BINARY, BYTE_ARRAY_NAME);
		MAP.put(Types.VARBINARY, BYTE_ARRAY_NAME);
		MAP.put(Types.LONGVARBINARY, BYTE_ARRAY_NAME);
		
		MAP.put(Types.BIT, BOOLEAN_NAME);
		
		MAP.put(Types.TINYINT, SHORT_NAME);
		MAP.put(Types.TINYINT, SHORT_NAME);
		MAP.put(Types.SMALLINT, SHORT_NAME);
		
		MAP.put(Types.INTEGER, INTEGER_NAME);
		
		MAP.put(Types.BIGINT, LONG_NAME);
		MAP.put(Types.REAL, FLOAT_NAME);
		MAP.put(Types.DOUBLE, DOUBLE_NAME);
		MAP.put(Types.DOUBLE, DOUBLE_NAME);
		
		MAP.put(Types.DECIMAL, MATH_BIGDECIMAL);// TODO Extraction using  ResultSet.getBigDecimal??
		MAP.put(Types.NUMERIC, MATH_BIGDECIMAL);
		
		MAP.put(Types.DATE, DATE_NAME);
		MAP.put(Types.TIME, DATE_NAME);
		MAP.put(Types.TIMESTAMP, DATE_NAME);
	}
}

package pl.edu.agh.grafken.lib.ui.console.commands;

import java.util.ArrayList;
import java.util.Scanner;

import pl.edu.agh.grafken.lib.GrafkenDaemon;
import pl.edu.agh.grafken.lib.api.user.IMapping;
import pl.edu.agh.grafken.lib.api.user.IObjectMetaInformation;
import pl.edu.agh.grafken.lib.api.user.IObjectsCache;
import pl.edu.agh.grafken.lib.api.user.IUserObject;
import pl.edu.agh.grafken.lib.ui.console.ConsoleApplication;
import pl.edu.agh.grafken.lib.utils.ArrayUtils;
import dnl.utils.text.table.TextTable;

public class GetObjectsByTypeNameCommand extends AbstractCommand implements
		ICommand {
	
	private Scanner scanner;
	private String databaseName;

	public GetObjectsByTypeNameCommand(Scanner scanner, String databaseName) {
		this.scanner = scanner;
		this.databaseName = databaseName;
	}

	@Override
	public void execute() {
		IMapping mapping = GrafkenDaemon.getInstance().getMappingManager().getMappingForDatabase(databaseName);
		if (mapping != null && mapping.isValid()) {
			new PrintMappedObjectTypesCommand(mapping, false).execute();

			System.out
					.println("Please enter which object you would like query for (name, maxResultNumber):");
			String objectName = scanner.nextLine();
			int maxResultNumber = Integer.parseInt(scanner.nextLine());

			IObjectMetaInformation meta = mapping
					.getObjectMetaInformation(objectName);
			if (meta != null) {
				IObjectsCache objectsHolder = meta.getObjects(maxResultNumber);
				
				ArrayList<IUserObject> allObjects = objectsHolder
						.getAllObjects();

				String header[] = new String[meta.getFieldsNames().size()];
				header = meta.getFieldsNames().toArray(header);
				TextTable tt = new TextTable(header,
						ArrayUtils.createArray2d(allObjects));
				tt.setAddRowNumbering(true);
				tt.printTable();
				System.out.println("Do you want to save data to clipboard? (yes/no)");
				String tmp = scanner.nextLine().toLowerCase().trim();
				meta.setObjectsCache(objectsHolder);
				if (tmp.equals("yes")){
					ConsoleApplication.getInstance().setClipboard(objectsHolder);
				}
			} else {
				// TODO: What else
			}

		} else {
			// TODO: Write what else
		}

	}
}

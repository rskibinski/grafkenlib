package pl.edu.agh.grafken.lib.database.schema.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Column")
public abstract class AbstractDatabaseColumn implements IDatabaseColumn, Comparable<AbstractDatabaseColumn> {

	private IDatabaseTable table;

	@XStreamAlias("Name")
	private String columnName;

	@XStreamAlias("Type")
	private Class<Object> dataType;

	@XStreamAlias("Mapping")
	private String mappingConfiguration;

	public AbstractDatabaseColumn(String columnName, IDatabaseTable tableName) {
		this.columnName = columnName;
		dataType = Object.class;
		mappingConfiguration = "";
		this.table = tableName;
	}

	@Override
	public String getColumnName() {
		return columnName;
	}

	@Override
	public String getMapping() {
		return mappingConfiguration;
	}
	@Override
	public IDatabaseTable getTable(){
		return table;
	}
	@Override
	public String getTableName() {
		return table.getTableName();
	}

	@Override
	public int compareTo(AbstractDatabaseColumn arg0) {
		if(arg0.getColumnName() == this.getColumnName() && arg0.getTableName() == this.getTableName()){
			return 0;
		}
		return arg0.getColumnName().compareTo(this.getColumnName());
	}

}

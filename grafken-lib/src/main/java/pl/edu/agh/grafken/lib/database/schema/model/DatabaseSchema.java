package pl.edu.agh.grafken.lib.database.schema.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Schema")
public class DatabaseSchema implements IDatabaseSchema {

	@XStreamAlias("Name")
	private String databaseID;

	/**
	 * Database schema name.
	 */
	private String schemaName;

	private Map<String, IDatabaseTable> tables;

	public DatabaseSchema(String databaseName) {
		this.databaseID = databaseName;
		tables = new HashMap<String, IDatabaseTable>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see database.schema.model.IDatabaseSchema#getDatabaseName()
	 */
	@Override
	public String getDatabaseID() {
		return databaseID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see database.schema.model.IDatabaseSchema#getDatabaseTables()
	 */
	@Override
	public Collection<IDatabaseTable> getDatabaseTables() {
		return tables.values();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * database.schema.model.IDatabaseSchema#addTable(database.schema.model.
	 * DatabaseTable)
	 */
	@Override
	public void addTable(IDatabaseTable table) {
		tables.put(table.getTableName(), table);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see database.schema.model.IDatabaseSchema#getTable(java.lang.String)
	 */
	@Override
	public IDatabaseTable getTable(String tableName) {
		return tables.get(tableName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * database.schema.model.IDatabaseSchema#getDatabaseColumn(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public IDatabaseColumn getDatabaseColumn(String tableName, String columnName) {
		IDatabaseTable table = getTable(tableName);
		if (table != null) {
			IDatabaseColumn column = table.getColumn(columnName);
			return column;
		} else {
			return null;
		}
	}

	@Override
	public String getSchemaName() {
		return schemaName;
	}

}

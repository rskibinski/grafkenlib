package pl.edu.agh.grafken.lib.ui.console.commands;

import java.util.ArrayList;
import java.util.Scanner;

import pl.edu.agh.grafken.lib.api.user.IObjectMetaReference;
import pl.edu.agh.grafken.lib.api.user.IObjectsCache;
import pl.edu.agh.grafken.lib.api.user.IUserObject;
import pl.edu.agh.grafken.lib.ui.console.ConsoleApplication;

public class GetObjectDependencyCommand implements ICommand {

	private Scanner scanner;

	public GetObjectDependencyCommand(Scanner scanner) {
		this.scanner = scanner;
	}

	@Override
	public void execute() {
		IObjectsCache clipboard = ConsoleApplication.getInstance()
				.getClipboard();
		if (clipboard != null) {
			new ShowClipboardCommand().execute();
		} else {
			System.out
					.println("Clipboard is empty - please load with some data first");
			return;
		}
		System.out
				.println("Please choose object to print details by index number ( 0 - "
						+ clipboard.getAllObjects().size() + " )");
		int index = Integer.parseInt(scanner.nextLine()); 
		IUserObject userObject = clipboard.getAllObjects()
				.get(index);

		System.out
				.println("Please choose which depedency you would like to query for:");
		ArrayList<IObjectMetaReference> allConnectedObjectsMetaInformation = new ArrayList<>(
				clipboard.getMeta().getConnectedObjectsMetaInformation());
		for (int i = 0; i < allConnectedObjectsMetaInformation.size(); i++) {
			System.out.println(i
					+ ". "
					+ allConnectedObjectsMetaInformation.get(i)
							.getForwardReferenceMeta().getName());
		}
		index = Integer.parseInt(scanner.nextLine());
		//
		IObjectMetaReference metaForeignKeyReference = allConnectedObjectsMetaInformation
				.get(index);
		IObjectsCache objects = metaForeignKeyReference
				.getObjectsConnected(userObject);
		for (IUserObject object : objects.getAllObjects()) {
			System.out.println(object.getValuesList());
		}
		ConsoleApplication.getInstance().setClipboard(objects);
		//

		// String information = userObject.getForwardBindings().get(index);
		//
		// IObjectMetaForeignKeyReference reference =
		// allConnectedObjectsMetaInformation.get(index);
		// IObjectsCache objectsConnected =
		// QueryBuilder.getInstance().getObjectsConnected(reference,
		// databaseName, information);
		// System.out.println(objectsConnected.getMeta().getFieldsNames());
		// ArrayList<IUserObject> objects = objectsConnected.getAllObjects();
	}

}

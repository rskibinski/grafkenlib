package pl.edu.agh.grafken.lib.database.schema.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import pl.edu.agh.grafken.lib.api.admin.IDatabaseEntity;
import pl.edu.agh.grafken.lib.api.user.IObjectMetaInformation;
import pl.edu.agh.grafken.lib.api.user.IObjectMetaReference;
import pl.edu.agh.grafken.lib.database.exceptions.NoKeysInTableException;
import pl.edu.agh.grafken.lib.database.schema.model.DatabaseColumn;
import pl.edu.agh.grafken.lib.database.schema.model.ForeignKeyColumn;
import pl.edu.agh.grafken.lib.database.schema.model.IDatabaseColumn;
import pl.edu.agh.grafken.lib.database.schema.model.IDatabaseSchema;
import pl.edu.agh.grafken.lib.database.schema.model.IDatabaseTable;
import pl.edu.agh.grafken.lib.database.schema.model.IForeignKeyColumn;
import pl.edu.agh.grafken.lib.database.schema.model.IPrimaryKeyColumn;
import pl.edu.agh.grafken.lib.mapping.model.Mapping;
import pl.edu.agh.grafken.lib.mapping.model.ObjectMetaForeignKeyReference;
import pl.edu.agh.grafken.lib.mapping.model.ObjectMetaInformation;
import pl.edu.agh.grafken.lib.utils.CustomStringBuilder;

/**
 * Serialize and deserialize database schema to file, allowing user to configure
 * mapping.
 * 
 * 
 */
public class SchemaSerializer {

	private static final Logger log = Logger.getLogger(SchemaSerializer.class);

	private static final String INDENT[] = { "", "\t", "\t\t", "\t\t\t" };

	private static final String FOREIGN_KEY_PATTERN = "\\^(\\w+)\\((\\w+):(\\w+)->(\\w+):(\\w+)\\)";

	private static final String CONFIGURATION_SEPARATOR = "###";

	private static Integer lineNumber = 0;

	/**
	 * Write database schema to file in easy to modify form.
	 * 
	 * @param db
	 *            database
	 * @param filePath
	 *            file path
	 * @throws IOException
	 */
	public static void marshallToCustomForm(IDatabaseEntity db, String filePath)
			throws IOException {
		String customForm = marshallToCustomForm(db);
		FileUtils.writeStringToFile(new File(filePath), customForm);
	}

	/**
	 * Returns mapping empty configuration.
	 * 
	 * @param db
	 *            database
	 * @throws IOException
	 */
	public static String marshallToCustomForm(IDatabaseEntity db) {
		IDatabaseSchema schema = SchemaFactory.createSchema(db);

		CustomStringBuilder writer = new CustomStringBuilder();
		for (IDatabaseTable table : schema.getDatabaseTables()) {
			writer.appendLine(INDENT[1] + table.getTableName());

			for (IPrimaryKeyColumn pkColumn : table.getPrimaryKeys()) {
				writer.appendLine(INDENT[2] + pkColumn.getColumnName()
						+ ":primaryKey");
			}
			writer.appendLine(INDENT[2] + "#");
			for (IDatabaseColumn column : table.getColumns()) {
				StringBuilder line = null;
				if (column instanceof DatabaseColumn) {
					line = new StringBuilder(INDENT[2] + column.getColumnName());
				} else if (column instanceof ForeignKeyColumn) {
					line = new StringBuilder(
							INDENT[2]
									+ column.getColumnName()
									+ " -> "
									+ (((ForeignKeyColumn) column)
											.getForeignTableName())
									+ "("
									+ (((ForeignKeyColumn) column)
											.getForeignTableColumn()) + ")"

					);
				}

				writer.appendLine(line.toString());
			}
		}
		writer.appendLine(CONFIGURATION_SEPARATOR);
		return writer.toString();
	}

	/**
	 * Reads database schema with user added mapping and returns mapping
	 * configuration.
	 * 
	 * @param db
	 *            database for which mapping is set
	 * @param filePath
	 *            file path
	 * @return Mapping configuration for database.
	 * @throws NoKeysInTableException
	 * @throws IOException
	 */
	public static Mapping unmarshallFromCustomForm(IDatabaseEntity db, 
			String filePath) throws NoKeysInTableException {
		// initialization
		IDatabaseSchema schema = SchemaFactory.createSchema(db);
		BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(filePath)));
			String line;
			lineNumber = 0;
			// skipping to configuration part
			do {
				line = reader.readLine();
				lineNumber++;
			} while (!line.equals(CONFIGURATION_SEPARATOR));
			Mapping mappingParent = new Mapping(db.getId());
			// configuration part
			while ((line = reader.readLine()) != null) {
				IObjectMetaInformation userObject;
				if (mappingParent.getObjectMetaInformation(line) == null) {
					userObject = new ObjectMetaInformation(mappingParent);
					userObject.setName(line);
					 log.debug("No object of that name exist - creating new with name: "
					 + line); 
				} else {
					userObject = mappingParent
							.getObjectMetaInformation(line);
					 log.debug("Object with name " + line +
					 " existed and was retrieved from cache."); 
				}

				// field initialization loop

				while ((line = reader.readLine()) != null
						&& line.startsWith(INDENT[1])) {
					reader.mark(1000);
					initializeObjectFields(mappingParent, userObject, line,
							schema);
				}
				initializeUniqueIdField(userObject, schema);
				mappingParent.addUserObject(userObject);
				
				reader.reset();

			}

			// finishing part
			reader.close();
			if (mappingParent.isValid()) {
				return mappingParent;
			}
		} catch (FileNotFoundException e) {
			log.error("Can't find mapping file for database: " + db.getId()
					+ " on path: " + filePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static void initializeObjectFields(Mapping mappingContainer,
			IObjectMetaInformation userObject, String line,
			IDatabaseSchema schema) throws NoKeysInTableException {
		if (line.trim().startsWith("^")) {
			initializeForeignKeyField(mappingContainer, userObject, line,
					schema);
		} else {
			initializeOrdinaryField(userObject, line, schema);
		}
	}

	private static void initializeOrdinaryField(
			IObjectMetaInformation userObject, // TODO: regexes
			String line, IDatabaseSchema schema) {
		String[] split = line.trim().split(":");
		Integer priority = null;
		String userObjectName = null;

		if (split.length < 2) {
			return; // TODO: Some notification mechanism
		} else if (split.length == 2) {
			userObjectName = split[1];
			priority = 1;
		} else if (split.length == 3) {
			userObjectName = split[2];
			priority = 1;
		} else if (split.length >= 4) {
			userObjectName = split[2];
			try {
				priority = Integer.parseInt(split[3]);
			} catch (NumberFormatException e) {
				priority = 1;
			}
		}
		IDatabaseColumn column = schema.getDatabaseColumn(getTableName(line),
				getColumnName(line));
		if (column != null) {
			userObject.addColumnAsField(column, userObjectName, priority);
		} else {
			return;
		}
	}

	private static String getTableName(String line) {
		String[] split = line.trim().split(":");
		return split[0];
	}

	private static String getColumnName(String line) {
		String[] split = line.trim().split(":");
		return split[1];
	}

	private static void initializeUniqueIdField(IObjectMetaInformation meta,
			IDatabaseSchema schema) throws NoKeysInTableException {
		Collection<Collection<IDatabaseColumn>> values = meta.getFieldsMapping()
				.values();
		for (Collection<IDatabaseColumn> outer : values) {
			for (IDatabaseColumn inner : outer) {
				Collection<IPrimaryKeyColumn> primaryKeys = inner.getTable()
						.getPrimaryKeys();
				if (primaryKeys != null && !primaryKeys.isEmpty()) {
					meta.addKeyColumns(primaryKeys);
				} else {
					Collection<IForeignKeyColumn> foreignKeys = inner
							.getTable().getForeignKeys();
					if (foreignKeys != null && !foreignKeys.isEmpty()) {
						meta.addKeyColumns(foreignKeys);
					} else {
						throw new NoKeysInTableException();
					}
				}
			}
		}
	}

	private static void initializeForeignKeyField(Mapping mapping,
			IObjectMetaInformation userObject, String line,
			IDatabaseSchema schema) {

		Pattern p = Pattern.compile(FOREIGN_KEY_PATTERN);
		Matcher matcher = p.matcher(line);
		if (matcher.find() && matcher.groupCount() == 5) {
			IObjectMetaInformation objectConnected;
			if (mapping.getObjectMetaInformation(matcher.group(1)) != null) {
				objectConnected = mapping.getObjectMetaInformation(matcher
						.group(1));
			} else {
				objectConnected = new ObjectMetaInformation(mapping);
				objectConnected.setName(matcher.group(1));
				mapping.addUserObject(objectConnected);
			}
			IObjectMetaReference objectReference = new ObjectMetaForeignKeyReference(
					objectConnected, userObject, matcher.group(4),
					matcher.group(5), matcher.group(2), matcher.group(3));
			objectConnected.addBackwardReference(objectReference);
			userObject.addConnectedObject(objectReference);
		} else {
			// TODO: handle wrong line
			 log.error("Wrong line pattern at " + lineNumber + " : " +
			 line); 
			return;
		}
	}

}

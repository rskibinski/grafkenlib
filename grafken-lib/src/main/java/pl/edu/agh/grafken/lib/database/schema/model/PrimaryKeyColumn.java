package pl.edu.agh.grafken.lib.database.schema.model;

public class PrimaryKeyColumn extends AbstractDatabaseColumn implements
		IPrimaryKeyColumn {

	public PrimaryKeyColumn(String columnName, IDatabaseTable tableName) {
		super(columnName, tableName);
	}

}

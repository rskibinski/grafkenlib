package pl.edu.agh.grafken.lib.ui.console.menu;

import pl.edu.agh.grafken.lib.ui.console.commands.ICommand;

public class MenuEntry {
	private String message;
	private Integer mnemonic;
	private ICommand command;

	public MenuEntry(String message, Integer mnemonic, ICommand command) {
		this.message = message;
		this.mnemonic = mnemonic;
		this.command = command;
	}

	public Integer getMnemonic() {
		return mnemonic;
	}

	public String getEntryString() {
		return getEntryString(1);
	}

	private String getEntryString(int indentationLevel) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < indentationLevel; i++) {
			sb.append("\t");
		}
		sb.append(mnemonic).append(". ").append(message).append("\n");
		return sb.toString();
	}

	public void execute() {
		command.execute();
	}
}

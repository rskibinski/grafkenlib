package pl.edu.agh.grafken.lib.ui.console.menu;

import java.util.Scanner;

import pl.edu.agh.grafken.lib.GrafkenDaemon;
import pl.edu.agh.grafken.lib.api.admin.IDatabaseEntity;
import pl.edu.agh.grafken.lib.api.admin.IMappingManager;
import pl.edu.agh.grafken.lib.api.user.IMapping;
import pl.edu.agh.grafken.lib.ui.console.commands.GetObjectDependencyCommand;
import pl.edu.agh.grafken.lib.ui.console.commands.GetObjectsByTypeNameCommand;
import pl.edu.agh.grafken.lib.ui.console.commands.LoadMappingFileCommand;
import pl.edu.agh.grafken.lib.ui.console.commands.MenuCommand;
import pl.edu.agh.grafken.lib.ui.console.commands.PrintMappedObjectTypesCommand;
import pl.edu.agh.grafken.lib.ui.console.commands.SearchForObjectsCommand;

public class UserMenu extends AbstractMenu implements IMenu {

	public UserMenu(Scanner scanner) {
		super(scanner);
		int i = 1;
		for (IDatabaseEntity db : GrafkenDaemon.getAdminInstance()
				.getDatabasesManager().getDatabases()) {
			IMapping mapping = GrafkenDaemon.getInstance().getMappingManager()
					.getMappingForDatabase(db.getId());
			if (mapping != null && mapping.isValid()) {
				entries.put(i, new MenuEntry(db.getId(), i, new MenuCommand(
						new DatabaseMenu(scanner, db.getId()))));
				i++;
			} else { // TODO: Check if it's even reachable
				entries.put(i, new MenuEntry(db.getId()
						+ " - no valid mapping. Choose to load file", i,
						new LoadMappingFileCommand(scanner, db)));
			}
		}
	}

	public class DatabaseMenu extends AbstractMenu {

		public DatabaseMenu(Scanner scanner, String databaseName) {
			super(scanner);
			IMappingManager mappingManager = GrafkenDaemon.getInstance()
					.getMappingManager();
			entries.put(
					1,
					new MenuEntry(
							"Print defined objects(plain)",
							1,
							new PrintMappedObjectTypesCommand(mappingManager
									.getMappingForDatabase(databaseName), false)));
			entries.put(
					2,
					new MenuEntry("Print defined objects(detailed)", 2,
							new PrintMappedObjectTypesCommand(mappingManager
									.getMappingForDatabase(databaseName), true)));
			entries.put(3, new MenuEntry("Get objects by object name", 3,
					new GetObjectsByTypeNameCommand(scanner, databaseName)));
			entries.put(4, new MenuEntry("Get object depedency", 4,
					new GetObjectDependencyCommand(scanner)));
			entries.put(5, new MenuEntry("Search for object", 5,
					new SearchForObjectsCommand(scanner, databaseName)));
		}
	}

}

package pl.edu.agh.grafken.lib.mapping.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import pl.edu.agh.grafken.lib.api.user.IMapping;
import pl.edu.agh.grafken.lib.api.user.IObjectMetaInformation;
import pl.edu.agh.grafken.lib.api.user.IObjectMetaReference;
import pl.edu.agh.grafken.lib.api.user.IObjectsCache;
import pl.edu.agh.grafken.lib.database.exceptions.NotSuchDatabaseInitializedException;
import pl.edu.agh.grafken.lib.database.schema.model.IDatabaseColumn;
import pl.edu.agh.grafken.lib.mapping.utils.QueryBuilder;

public class ObjectMetaInformation implements IObjectMetaInformation {

	private final Logger log = Logger.getLogger(ObjectMetaInformation.class);

	private String name;
	/**
	 * Mapping between field name and collection of columns which after
	 * concatenation creates field.
	 */
	private Map<String, Collection<IDatabaseColumn>> mapping = new HashMap<String, Collection<IDatabaseColumn>>();

	private TreeMap<String, IObjectMetaReference> objectsConnectedMeta = new TreeMap<>();

	private TreeMap<String, IObjectMetaReference> backwardReferences = new TreeMap<>();

	private TreeSet<IDatabaseColumn> uniqueIdColumnsSet = new TreeSet<>();

	private IObjectsCache objectsCache = new ObjectsCache(this);

	private Mapping parent;

	public ObjectMetaInformation(Mapping parent) {
		this.parent = parent;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addColumnAsField(IDatabaseColumn column, String fieldName,
			int priority) {
		if (!mapping.containsKey(fieldName)) {
			mapping.put(fieldName, new ArrayList<IDatabaseColumn>());
		}

		Collection<IDatabaseColumn> collection = mapping.get(fieldName);
		collection.add(column);
		// TODO: Sort by priority
	}

	@Override
	public void addKeyColumn(IDatabaseColumn column) {
		uniqueIdColumnsSet.add(column);
	}

	@Override
	public void addKeyColumns(Collection<? extends IDatabaseColumn> columns) {
		uniqueIdColumnsSet.addAll(columns);
	}

	@Override
	public ArrayList<IDatabaseColumn> getUidColumns() {
		return new ArrayList<>(uniqueIdColumnsSet);
	}

	@Override
	public Map<String, Collection<IDatabaseColumn>> getFieldsMapping() {
		return mapping;
	}

	public void setMapping(Map<String, Collection<IDatabaseColumn>> mapping) {
		this.mapping = mapping;
	}

	@Override
	public ArrayList<String> getFieldsNames() {
		return new ArrayList<String>(mapping.keySet());
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void addConnectedObject(IObjectMetaReference objectConnected) {
		log.info("[" + this.name + "] " + "Adding connected object "
				+ objectConnected.getForwardReferenceMeta().getName() + " to "
				+ objectConnected.getBackwardReferenceMeta().getName());
		objectsConnectedMeta.put(objectConnected.getForwardReferenceMeta()
				.getName(), objectConnected);
	}

	@Override
	public IObjectMetaReference getConnectedObjectMeta(String objectID) {
		return objectsConnectedMeta.get(objectID);
	}

	@Override
	public Collection<IObjectMetaReference> getConnectedObjectsMetaInformation() {
		return objectsConnectedMeta.values(); // TODO: return immutable?
	}

	@Override
	public void addBackwardReference(IObjectMetaReference objectReference) {
		log.info("[" + this.name + "] " + "Adding backward reference object "
				+ objectReference.getForwardReferenceMeta().getName() + " to "
				+ objectReference.getBackwardReferenceMeta().getName());
		backwardReferences.put(objectReference.getBackwardReferenceMeta()
				.getName(), objectReference);
	}

	@Override
	public Collection<IObjectMetaReference> getBackwardReferences() {
		return backwardReferences.values();
	}

	@Override
	public IObjectsCache getObjectsCache() {
		return objectsCache;
	}

	@Override
	public void setObjectsCache(IObjectsCache objectsHolder) {
		this.objectsCache = objectsHolder;
	}

	@Override
	public int getReferenceIndex(String key) {
		String[] mapKeys = new String[objectsConnectedMeta.size()];
		objectsConnectedMeta.keySet().toArray(mapKeys);
		int index = Arrays.binarySearch(mapKeys, key);
		return index;
	}

	@Override
	public IMapping getParent() {
		return parent;
	}

	@Override
	public IObjectsCache searchForObject(String value)
			throws NotSuchDatabaseInitializedException {
		IObjectsCache results = QueryBuilder.getInstance().searchForObject(
				this, value);
		objectsCache.addObjects(results.getAllObjects());
		return objectsCache.getObjectsSubCache(results.getKeys());
	}

	@Override
	public IObjectsCache getObjects(int maxResultNumber) {
		return getObjects(maxResultNumber, 0);
	}

	@Override
	public IObjectsCache getObjects(int maxResultNumber, int offset) {
		IObjectsCache results = QueryBuilder.getInstance().getObjectData(this,
				getParent().getDatabaseID(), maxResultNumber, offset);
		objectsCache.addObjects(results.getAllObjects());
		return objectsCache.getObjectsSubCache(results.getKeys());
	}

	@Override
	public Collection<String> getConnectedObjectsNames() {
		ArrayList<String> result = new ArrayList<>();
		for(IObjectMetaReference meta: getConnectedObjectsMetaInformation()){
			result.add(meta.getForwardReferenceMeta().getName());
		}
		return result;
	}

}

package pl.edu.agh.grafken.lib;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import pl.edu.agh.grafken.lib.api.admin.AdminApi;
import pl.edu.agh.grafken.lib.api.admin.IDatabaseConfigurator;
import pl.edu.agh.grafken.lib.api.admin.IDatabaseEntity;
import pl.edu.agh.grafken.lib.api.admin.IDatabaseManager;
import pl.edu.agh.grafken.lib.api.admin.IMappingManager;
import pl.edu.agh.grafken.lib.api.user.UserApi;
import pl.edu.agh.grafken.lib.database.exceptions.ConfigurationParsingException;
import pl.edu.agh.grafken.lib.database.exceptions.NoKeysInTableException;
import pl.edu.agh.grafken.lib.database.model.DatabaseManager;
import pl.edu.agh.grafken.lib.database.schema.utils.SchemaSerializer;
import pl.edu.agh.grafken.lib.database.utils.DatabaseConfigurator;
import pl.edu.agh.grafken.lib.mapping.model.Mapping;
import pl.edu.agh.grafken.lib.mapping.model.MappingManager;
import pl.edu.agh.grafken.lib.ui.console.ConsoleApplication;

/**
 * 
 *
 */
public class GrafkenDaemon implements AdminApi, UserApi {

	static String input = "";
	
	static final Logger log = Logger.getLogger(GrafkenDaemon.class);

	private DatabaseManager dbManager;
	
	private IMappingManager mappingManager;
	
	private static GrafkenDaemon application;
	
	public static AdminApi getAdminInstance() {
		if (application == null) {
			application = new GrafkenDaemon(new DatabaseConfigurator());
		}
		return application;
	}

	public static UserApi getUserInstance() {
		if (application == null) {
			application = new GrafkenDaemon(new DatabaseConfigurator());
		}
		return application;
	}

	public static GrafkenDaemon getInstance() {
		if (application == null) {
			application = new GrafkenDaemon(new DatabaseConfigurator());
		}
		return application;
	}

	private GrafkenDaemon(IDatabaseConfigurator databaseConfigurator) {
		this.dbManager = new DatabaseManager(databaseConfigurator);
		this.mappingManager = new MappingManager();

	}

	@Override
	public void init() {
		log.info("Application started");
		try {
			dbManager.initializeDatabases();
		} catch (ConfigurationParsingException e1) {
			terminate(e1);
		}
		ArrayList<IDatabaseEntity> databases = dbManager.getDatabases();
		log.info("Finished database connection pools configuration, configured databases: "
				+ databases.toString());
		for (IDatabaseEntity db : databases) {
			Mapping mappingContainer;
			try {
				mappingContainer = SchemaSerializer
						.unmarshallFromCustomForm(db, dbManager
								.getDefaultMappingFilePathForDatabase(db.getId()));
				if (mappingContainer != null) {
					mappingManager.addMapping(mappingContainer);
				}
			} catch (NoKeysInTableException e) {
				e.printStackTrace();
			}
		}
		log.info("Finished on start application routines");
	}

	private void terminate(Exception e1) {
		log.fatal("Terminating application due to fatal error", e1);
		shutdown();
		System.exit(1);
	}

	public void shutdown() {
		dbManager.shutdownDatabases();
		mappingManager.reset();
	}
	
	public void reset(){
		shutdown();
		init();
	}

	@Override
	public IDatabaseManager getDatabasesManager() {
		return dbManager;
	}

	public void setDatabaseConfigurator(
			IDatabaseConfigurator databaseConfigurator) {
		dbManager.setDatabaseConfigurator(databaseConfigurator);
	}

	public IMappingManager getMappingManager() {
		return mappingManager;
	}
	
	public void startConsole(){
		ConsoleApplication.getInstance().repl();
	}
}

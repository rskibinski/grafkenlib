package pl.edu.agh.grafken.lib.ui.console.commands;

import java.util.Scanner;

import pl.edu.agh.grafken.lib.GrafkenDaemon;
import pl.edu.agh.grafken.lib.api.admin.IDatabaseEntity;
import pl.edu.agh.grafken.lib.database.exceptions.NoKeysInTableException;
import pl.edu.agh.grafken.lib.database.schema.utils.SchemaSerializer;
import pl.edu.agh.grafken.lib.mapping.model.Mapping;
import pl.edu.agh.grafken.lib.utils.StringUtils;

public class LoadMappingFileCommand extends AbstractCommand {

	private Scanner scanner;
	private IDatabaseEntity db;

	public LoadMappingFileCommand(Scanner scanner, IDatabaseEntity db) {
		this.scanner = scanner;
		this.db = db;
	}

	@Override
	public void execute() {
		System.out.println("Enter mapping filepath for database " + StringUtils.quoteArgument(db.getId()));
		String filePath = scanner.nextLine();
		Mapping newMapping;
		try {
			newMapping = SchemaSerializer.unmarshallFromCustomForm(db, filePath);
			if(newMapping.isValid()){
				LOG.info("Successfully loaded configuration file : " + StringUtils.quoteArgument(filePath) + " for database: " + StringUtils.quoteArgument(db.getId()));
			}
			GrafkenDaemon.getInstance().getMappingManager().addMapping(newMapping);
		} catch (NoKeysInTableException e) {
			e.printStackTrace();
		}
	}

}

package pl.edu.agh.grafken.lib.mapping.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import pl.edu.agh.grafken.lib.api.user.IObjectMetaReference;
import pl.edu.agh.grafken.lib.api.user.IObjectMetaInformation;
import pl.edu.agh.grafken.lib.api.user.IObjectsCache;
import pl.edu.agh.grafken.lib.api.user.IUserObject;
import pl.edu.agh.grafken.lib.mapping.utils.QueryBuilder;
import pl.edu.agh.grafken.lib.utils.KeyImmutableList;
import pl.edu.agh.grafken.lib.wrapper.interfaces.IObjectMetaInformationWrapper;

public class ObjectMetaForeignKeyReference implements
		IObjectMetaReference {

	private String foreignTableName;

	private String foreignColumnName;

	private String originTableName;

	private String originColumnName;

	private IObjectMetaInformation forwardReference;

	private IObjectMetaInformation backwardReference;

	private HashMap<KeyImmutableList<String>, Set<KeyImmutableList<String>>> cacheMap = new HashMap<>();

	public ObjectMetaForeignKeyReference(
			IObjectMetaInformation forwardReference,
			IObjectMetaInformation backwardReference, String foreignTableName,
			String foreignColumnName, String originTableName,
			String originColumnName) {
		this.foreignTableName = foreignTableName;
		this.foreignColumnName = foreignColumnName;
		this.originTableName = originTableName;
		this.originColumnName = originColumnName;
		this.forwardReference = forwardReference;
		this.backwardReference = backwardReference;
	}

	@Override
	public String getForeignTableName() {
		return foreignTableName;
	}

	@Override
	public String getForeignColumnName() {
		return foreignColumnName;
	}

	@Override
	public String getOriginTableName() {
		return originTableName;
	}

	@Override
	public String getOriginColumnName() {
		return originColumnName;
	}

	@Override
	public IObjectMetaInformation getForwardReferenceMeta() {
		return forwardReference;
	}

	@Override
	public IObjectMetaInformation getBackwardReferenceMeta() {
		return backwardReference;
	}

	@Override
	public String toString() {
		return getBackwardReferenceMeta().getName() + " -> "
				+ getForwardReferenceMeta().getName() + " ( "
				+ getOriginTableName() + ":" + getOriginColumnName() + " -> "
				+ getForeignTableName() + ":" + getForeignColumnName() + ")";
	}

	@Override
	public IObjectsCache getObjectsConnected(IUserObject userObject) {
		// For now, we each db each time and then update caches with new
		// objects.
		// ArrayList<IUserObject> objectsFromCache =
		// getObjectsFromCache(userObject);
		// Set<KeyImmutableList<String>> set2 =
		// cacheMap.get(userObject.getKey());
		// if (objectsFromCache != null) {
		// Set<KeyImmutableList<String>> set =
		// cacheMap.get(userObject.getKey());
		// } else {
		int referenceIndex = backwardReference
				.getReferenceIndex(getForwardReferenceMeta().getName());
		String binding = userObject.getForwardBindings().get(referenceIndex);
		IObjectsCache objectsConnected = QueryBuilder.getInstance()
				.getObjectsConnected(this,
						forwardReference.getParent().getDatabaseID(), binding);
		synchronized (this) {
			Set<KeyImmutableList<String>> set = cacheMap.get(userObject
					.getKey()) == null ? new HashSet<KeyImmutableList<String>>()
					: cacheMap.get(userObject.getKey());
			for (IUserObject ob : objectsConnected.getAllObjects()) {
				set.add(ob.getKey());
			}

			getForwardReferenceMeta().getObjectsCache().addObjects(
					objectsConnected.getAllObjects());
			IObjectsCache cache = getForwardReferenceMeta().getObjectsCache()
					.getObjectsSubCache(objectsConnected.getKeys());
			return cache;
		}
		// }
	}

	private ArrayList<IUserObject> getObjectsFromCache(IUserObject userObject) {
		Set<KeyImmutableList<String>> keySet = cacheMap
				.get(userObject.getKey());
		ArrayList<IUserObject> arrayList = new ArrayList<>();
		if (keySet != null) {
			IObjectsCache cache = getForwardReferenceMeta().getObjectsCache();
			for (KeyImmutableList<String> key : keySet) {
				IUserObject object = cache.getObject(key);
				arrayList.add(object);
			}
		}
		return arrayList.isEmpty() ? null : arrayList;
	}

}

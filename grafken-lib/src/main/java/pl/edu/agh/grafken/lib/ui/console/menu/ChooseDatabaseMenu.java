package pl.edu.agh.grafken.lib.ui.console.menu;

import java.util.Scanner;

import pl.edu.agh.grafken.lib.GrafkenDaemon;
import pl.edu.agh.grafken.lib.api.admin.IDatabaseEntity;
import pl.edu.agh.grafken.lib.ui.console.commands.GenerateMappingFileCommand;
import pl.edu.agh.grafken.lib.ui.console.commands.LoadMappingFileCommand;
import pl.edu.agh.grafken.lib.ui.console.commands.MenuCommand;

public class ChooseDatabaseMenu extends AbstractMenu {

	public ChooseDatabaseMenu(Scanner scanner) {
		super(scanner);
		int i = 1;
		for (IDatabaseEntity db : GrafkenDaemon.getAdminInstance().getDatabasesManager().getDatabases()) {
			entries.put(i, new MenuEntry(db.getId(), i, new MenuCommand(
					new MappingOptionsMenu(scanner, db))));
			i++;
		}
	}

	public class MappingOptionsMenu extends AbstractMenu {

		public MappingOptionsMenu(Scanner scanner, IDatabaseEntity db) {
			super(scanner);
			entries.put(1, new MenuEntry("Generate empty mapping file", 1,
					new GenerateMappingFileCommand(scanner, db)));
			 entries.put(2, new MenuEntry("Load mapping file", 2,
			 new LoadMappingFileCommand(scanner, db)));
		}

	}
}

package pl.edu.agh.grafken.lib.ui.console;

import java.io.InputStreamReader;
import java.util.Scanner;

import pl.edu.agh.grafken.lib.GrafkenDaemon;
import pl.edu.agh.grafken.lib.api.admin.AdminApi;
import pl.edu.agh.grafken.lib.api.user.IObjectsCache;
import pl.edu.agh.grafken.lib.api.user.UserApi;
import pl.edu.agh.grafken.lib.ui.console.menu.TopMenu;

public class ConsoleApplication implements UIApi {

	private static AdminApi adminInstance;

	private static UserApi userInstance;
	
	private IObjectsCache clipboard;
	
	private static ConsoleApplication instance;

	public static void main(String[] args) {
		instance = new ConsoleApplication();
		adminInstance = GrafkenDaemon.getAdminInstance();
		adminInstance.init();
		userInstance = GrafkenDaemon.getUserInstance();
		instance.repl();
	}
	
	public static ConsoleApplication getInstance(){
		if(instance == null){
			instance = new ConsoleApplication();
		}
		return instance;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pl.edu.agh.grafken.ui.console.UIApi#getAdminApplication()
	 */
	@Override
	public AdminApi getAdminApplication() {
		return adminInstance;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pl.edu.agh.grafken.ui.console.UIApi#getUserApplication()
	 */
	@Override
	public UserApi getUserApplication() {
		return userInstance;
	}

	public void repl() {
		TopMenu topMenu = new TopMenu(new Scanner(new InputStreamReader(
				System.in)));
		while (true)
			topMenu.takeControl(this);
	}
	
	public void setClipboard(IObjectsCache holder){
		this.clipboard = holder;
	}
	
	public IObjectsCache getClipboard(){
		return clipboard;
	}

}

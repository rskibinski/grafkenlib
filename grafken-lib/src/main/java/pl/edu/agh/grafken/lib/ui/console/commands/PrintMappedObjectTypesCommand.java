package pl.edu.agh.grafken.lib.ui.console.commands;

import pl.edu.agh.grafken.lib.api.user.IMapping;
import pl.edu.agh.grafken.lib.api.user.IObjectMetaInformation;

public class PrintMappedObjectTypesCommand extends AbstractCommand implements
		ICommand {

	private IMapping mapping;
	private boolean printDetails;

	public PrintMappedObjectTypesCommand(IMapping mapping2,
			boolean printDetails) {
		this.mapping = mapping2;
		this.printDetails = printDetails;
	}

	@Override
	public void execute() {
		if (mapping != null && mapping.isValid()) {
			System.out.println("Objects for "
					+ pl.edu.agh.grafken.lib.utils.StringUtils
							.quoteArgument(mapping.getDatabaseID()));
			for (IObjectMetaInformation object : mapping.getObjectsMeta().values()) {
				new PrintObjectMetaInformationCommand(object, printDetails).execute();
			}
		} else {
			System.out.println("Existing mapping is not valid for printing");
		}
	}
}

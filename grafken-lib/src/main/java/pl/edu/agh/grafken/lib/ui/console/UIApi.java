package pl.edu.agh.grafken.lib.ui.console;

import pl.edu.agh.grafken.lib.api.admin.AdminApi;
import pl.edu.agh.grafken.lib.api.user.UserApi;

public interface UIApi {

	public AdminApi getAdminApplication();

	public UserApi getUserApplication();
	

}
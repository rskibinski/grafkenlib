package pl.edu.agh.grafken.lib.database.schema.model;

public class DatabaseColumn extends AbstractDatabaseColumn {
	/**
	 * Creates an instance of common Database Column(not being FK or PK).
	 * 
	 * @param columnName
	 *            database column name
	 * @param tableName
	 *            databse table name
	 */
	public DatabaseColumn(String columnName, IDatabaseTable tableName) {
		super(columnName, tableName);
	}

}

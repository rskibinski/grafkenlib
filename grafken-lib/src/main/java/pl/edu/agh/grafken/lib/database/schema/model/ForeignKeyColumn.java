package pl.edu.agh.grafken.lib.database.schema.model;



public class ForeignKeyColumn extends AbstractDatabaseColumn implements
		IForeignKeyColumn {
	
	private String originColumnName;
	
	private String importedTableName;
	
	private String importedColumnName;
	

	public ForeignKeyColumn(String originColumnName, String importedTableName,
			String importedColumName, IDatabaseTable originTable) {
		super(originColumnName, originTable);
		this.importedColumnName = importedColumName;
		this.importedTableName = importedTableName;
	}

	@Override
	public String getForeignTableName() {
		return importedTableName;
	}

	@Override
	public String getForeignTableColumn() {
		return importedColumnName;
	}

	@Override
	public String getOriginColumnName() {
		return originColumnName;
	}


}

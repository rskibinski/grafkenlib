package pl.edu.agh.grafken.lib.database.schema.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("Table")
public class DatabaseTable implements IDatabaseTable{
	
	@XStreamAlias("Name")
	private String name;
	
	@XStreamImplicit(itemFieldName="columns")
	private Map<String, IDatabaseColumn> columns;
	
	private Map<String, IPrimaryKeyColumn> primaryKeyColumns;

	public DatabaseTable(String tableName) {
		name = tableName;
		columns = new HashMap<String, IDatabaseColumn>();
		primaryKeyColumns = new HashMap<>();
	}

	/* (non-Javadoc)
	 * @see database.schema.model.IDatabaseTable#getName()
	 */
	@Override
	public String getTableName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see database.schema.model.IDatabaseTable#getColumns()
	 */
	@Override
	public Collection<IDatabaseColumn> getColumns() {
		return columns.values();
	}

	/* (non-Javadoc)
	 * @see database.schema.model.IDatabaseTable#getPrimaryKeys()
	 */
	@Override
	public Collection<IPrimaryKeyColumn> getPrimaryKeys() {
		return primaryKeyColumns.values();
	}

	/* (non-Javadoc)
	 * @see database.schema.model.IDatabaseTable#getForeignKeys()
	 */
	@Override
	public Collection<IForeignKeyColumn> getForeignKeys() {
		// TODO Check if its necessary at all.
		return null;
	}

	/* (non-Javadoc)
	 * @see database.schema.model.IDatabaseTable#addColumn(database.schema.model.IDatabaseColumn)
	 */
	@Override
	public void addColumn(IDatabaseColumn column) {
		columns.put(column.getColumnName(), column);
	}
	
	/* (non-Javadoc)
	 * @see database.schema.model.IDatabaseTable#addPrimaryKeyColumn(database.schema.model.IPrimaryKeyColumn)
	 */
	@Override
	public void addPrimaryKeyColumn(IPrimaryKeyColumn column){
		primaryKeyColumns.put(column.getColumnName(), column);
	}

	/* (non-Javadoc)
	 * @see database.schema.model.IDatabaseTable#getColumn(java.lang.String)
	 */
	@Override
	public IDatabaseColumn getColumn(String columnName) {
		return columns.get(columnName);
	}

	/* (non-Javadoc)
	 * @see database.schema.model.IDatabaseTable#addForeignKeyColumn(database.schema.serialization.ForeignKeyColumn)
	 */
	@Override
	public void addForeignKeyColumn(IForeignKeyColumn foreignKeyColumn) {
		columns.put(foreignKeyColumn.getColumnName(), foreignKeyColumn);
	}
}

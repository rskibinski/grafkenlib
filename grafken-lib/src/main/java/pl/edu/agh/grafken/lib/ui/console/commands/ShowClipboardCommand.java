package pl.edu.agh.grafken.lib.ui.console.commands;

import java.util.ArrayList;

import pl.edu.agh.grafken.lib.api.user.IObjectMetaReference;
import pl.edu.agh.grafken.lib.api.user.IObjectsCache;
import pl.edu.agh.grafken.lib.api.user.IUserObject;
import pl.edu.agh.grafken.lib.ui.console.ConsoleApplication;
import dnl.utils.text.table.TextTable;

public class ShowClipboardCommand implements ICommand {

	@Override
	public void execute() {
		IObjectsCache clipboard = ConsoleApplication.getInstance().getClipboard();
		if(clipboard != null){
			ArrayList<IUserObject> allObjects = clipboard.getAllObjects();
			String [] header = new String[]{"Id", "UUID", "First field", "Connected objects IDs"};
			Object [][] values = new Object[allObjects.size()][4];
			for(int i=0;i<allObjects.size(); i++){
				values[i][0] = i;
				values[i][1] = allObjects.get(i).toString();
				values[i][2] = allObjects.get(i).getPropertyValue(0);
				StringBuilder connectedObjectIDs = new StringBuilder();
				for(IObjectMetaReference reference: clipboard.getMeta().getConnectedObjectsMetaInformation()){
					connectedObjectIDs.append(reference.getForwardReferenceMeta().getName()).append(", ");
				}
				values[i][3] = connectedObjectIDs.toString();
			}
			TextTable tt = new TextTable(header, values);
			tt.printTable();
		}
	}

}

package pl.edu.agh.grafken.lib.database.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import pl.edu.agh.grafken.lib.api.admin.IDatabaseConfigurator;
import pl.edu.agh.grafken.lib.api.admin.IDatabaseEntity;
import pl.edu.agh.grafken.lib.database.exceptions.ConfigurationParsingException;
import pl.edu.agh.grafken.lib.database.model.DatabaseEntity;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.StreamException;

/**
 * Parser class for reading and writing from/to filesystem database
 * configuration.
 * 
 */
public class DatabaseConfigurator implements IDatabaseConfigurator{
	protected Logger log = Logger.getLogger(this.getClass());

	protected String configurationDir = "conf";

	protected String databaseConfigurationPath;

	protected String mappingDirPath;

	public DatabaseConfigurator() {
		databaseConfigurationPath = new StringBuilder()
				.append(configurationDir).append(File.separator)
				.append("databases.conf").toString();
		mappingDirPath = new StringBuilder().append(configurationDir)
				.append(File.separator).append("mapping").toString();
	}

	/* (non-Javadoc)
	 * @see pl.edu.agh.grafken.lib.database.utils.IDatabaseConfigurator#parseFileForDatabaseConfigurations()
	 */
	@Override
	public ArrayList<? extends IDatabaseEntity> parseFileForDatabaseConfigurations()
			throws ConfigurationParsingException {
		return parseFileForDatabaseConfigurations(getConfigurationPath());
	}

	/* (non-Javadoc)
	 * @see pl.edu.agh.grafken.lib.database.utils.IDatabaseConfigurator#parseFileForDatabaseConfigurations(java.lang.String)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public ArrayList<? extends IDatabaseEntity> parseFileForDatabaseConfigurations(
			String filePath) throws ConfigurationParsingException {
		ArrayList<? extends IDatabaseEntity> fromXML = null;
		try {
			XStream xs = new XStream();
			xs.processAnnotations(DatabaseEntity.class);
			fromXML = (ArrayList<DatabaseEntity>) xs
					.fromXML(new File(filePath));
		} catch (StreamException e) {
			log.fatal("Can't parse database configuration file");
			throw new ConfigurationParsingException();
		}
		return fromXML;
	}

	/* (non-Javadoc)
	 * @see pl.edu.agh.grafken.lib.database.utils.IDatabaseConfigurator#dumpDatabaseConfiguration(java.util.ArrayList, java.lang.String)
	 */
	@Override
	public void dumpDatabaseConfiguration(ArrayList<IDatabaseEntity> dbs,
			String filePath) {
		XStream xs = new XStream();
		xs.processAnnotations(DatabaseEntity.class);
		try {
			xs.toXML(dbs, new FileOutputStream(new File(filePath)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public String dumpDatabaseConfiguration(ArrayList<IDatabaseEntity> dbs) {
		XStream xs = new XStream();
		xs.processAnnotations(DatabaseEntity.class);
		return xs.toXML(dbs);
	}

	/* (non-Javadoc)
	 * @see pl.edu.agh.grafken.lib.database.utils.IDatabaseConfigurator#getDefaultMappingFilePath(java.lang.String)
	 */
	@Override
	public String getDefaultMappingFilePath(String dbName) {
		return mappingDirPath + File.separator + dbName;
	}

	/* (non-Javadoc)
	 * @see pl.edu.agh.grafken.lib.database.utils.IDatabaseConfigurator#getConfigurationPath()
	 */
	@Override
	public String getConfigurationPath() {
		return databaseConfigurationPath;
	}
}

package pl.edu.agh.grafken.lib.ui.console.commands;


public class ExitCommand extends AbstractCommand {

	@Override
	public void execute() {
		System.exit(0);
	}

}

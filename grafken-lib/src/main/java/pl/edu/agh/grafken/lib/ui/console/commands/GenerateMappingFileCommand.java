package pl.edu.agh.grafken.lib.ui.console.commands;

import java.io.IOException;
import java.util.Scanner;

import pl.edu.agh.grafken.lib.api.admin.IDatabaseEntity;
import pl.edu.agh.grafken.lib.database.schema.utils.SchemaSerializer;

public class GenerateMappingFileCommand extends AbstractCommand {

	private IDatabaseEntity db;
	private Scanner scanner;

	public GenerateMappingFileCommand(Scanner scanner, IDatabaseEntity db) {
		this.scanner = scanner;
		this.db = db;
	}

	@Override
	public void execute() {
		System.out.println("Enter filepath(default .):");
		String line = scanner.nextLine();
		if (line.isEmpty())
			line = "./" + db.getId();
		try {
			SchemaSerializer.marshallToCustomForm(db, line);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

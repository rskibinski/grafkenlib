package pl.edu.agh.grafken.lib.ui.console.menu;

import pl.edu.agh.grafken.lib.ui.console.UIApi;

public interface IMenu {

	public void printMenu();

	public abstract void takeControl(UIApi applicationContext);
	
	public abstract UIApi getApplicationContext();

}
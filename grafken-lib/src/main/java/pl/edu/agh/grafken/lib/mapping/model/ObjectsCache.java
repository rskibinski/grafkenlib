package pl.edu.agh.grafken.lib.mapping.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import pl.edu.agh.grafken.lib.api.user.IObjectMetaInformation;
import pl.edu.agh.grafken.lib.api.user.IObjectsCache;
import pl.edu.agh.grafken.lib.api.user.IUserObject;
import pl.edu.agh.grafken.lib.utils.KeyImmutableList;

public class ObjectsCache implements IObjectsCache {

	private static final Logger log = Logger.getLogger(ObjectsCache.class);

	private IObjectMetaInformation meta;

	private HashMap<KeyImmutableList<String>, IUserObject> cache = new HashMap<>();

	public ObjectsCache(IObjectMetaInformation meta) {// TODO: exception on null?
		assert meta != null;
		this.meta = meta;
	}

	public ObjectsCache(IObjectMetaInformation metaData,
			Collection<IUserObject> objects) {
		this.meta = metaData;
		addObjects(objects);
	}

	public ArrayList<IUserObject> getAllObjects() {
		return new ArrayList<>(cache.values());
	}

	@Override
	public void add(IUserObject object) {
		KeyImmutableList<String> keyList = object.getKey();
		if (!(cache.containsKey(keyList))) {
			cache.put(keyList, object);
		} else {
			log.info("Element exists in cache: " + object.toString());
		}
	}

	@Override
	public IUserObject getObject(KeyImmutableList<String> key) {
		if(cache.containsKey(key)){
			log.info(key.toString() + " exists in cache");
		}
		return cache.get(key);
		
	}

	public void clear() {
		cache.clear();
	}

	@Override
	public void addObjects(Collection<IUserObject> allObjects) {
		for (IUserObject obj : allObjects) {
			add(obj);
		}
	}

	@Override
	public IObjectMetaInformation getMeta() {
		return meta;
	}

	@Override
	public Set<KeyImmutableList<String>> getKeys() {
		return cache.keySet();
	}

	@Override
	public Set<IUserObject> getObjectsSet(Set<KeyImmutableList<String>> keys) {
		Set<IUserObject> results = new HashSet<>();
		for(KeyImmutableList<String> key: keys){
			results.add(cache.get(key));
		}
		return results;
	}

	@Override
	public IObjectsCache getObjectsSubCache(Set<KeyImmutableList<String>> keys) {
		Set<IUserObject> objectsSet = getObjectsSet(keys);
		return new ObjectsCache(meta, objectsSet);
	}
}

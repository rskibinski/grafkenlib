package pl.edu.agh.grafken.lib.mapping.utils;

import pl.edu.agh.grafken.lib.api.user.IObjectMetaInformation;
import pl.edu.agh.grafken.lib.api.user.IObjectMetaReference;
import pl.edu.agh.grafken.lib.api.user.IObjectsCache;
import pl.edu.agh.grafken.lib.database.exceptions.NotSuchDatabaseInitializedException;

public interface IQueryBuilder {
	
	public IObjectsCache getObjectData(IObjectMetaInformation metaData, String databaseName);

	public abstract IObjectsCache getObjectsConnected(IObjectMetaReference ref, String databaseName,
			String information);

	public abstract IObjectsCache searchForObject(IObjectMetaInformation meta, String value)
			throws NotSuchDatabaseInitializedException;
}

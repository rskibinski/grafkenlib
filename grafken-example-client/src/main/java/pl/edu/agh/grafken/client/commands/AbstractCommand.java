package pl.edu.agh.grafken.client.commands;

import org.apache.log4j.Logger;

public abstract class AbstractCommand implements ICommand {
	
	protected Logger LOG = null;
	
	/* (non-Javadoc)
	 * @see pl.edu.agh.grafken.commands.ICommand#execute()
	 */
	@Override
	public abstract void execute();
	
	public AbstractCommand(){
		LOG = Logger.getLogger(this.getClass());
	}
}

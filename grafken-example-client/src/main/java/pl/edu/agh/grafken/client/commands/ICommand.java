package pl.edu.agh.grafken.client.commands;

public interface ICommand {

	public void execute();

}
package pl.edu.agh.grafken.client.commands.communication;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import pl.edu.agh.grafken.client.App;
import pl.edu.agh.grafken.client.commands.AbstractCommand;
import pl.edu.agh.grafken.client.commands.ICommand;

public class GetDatabaseEmtpyMappingCommand extends AbstractCommand implements
		ICommand {

	private String dbId;
	private String mapping;

	public GetDatabaseEmtpyMappingCommand(String dbId) {
		this.dbId = dbId;
	}

	@Override
	public void execute() {
		WebTarget target = App.getInstance().getClient()
				.target(App.getBaseURI()).path("mapping").path("getMapping")
				.path(dbId);
		Response response = target.request(MediaType.TEXT_PLAIN).get();
		if (response.getStatus() == Response.Status.OK.getStatusCode()) {
			mapping = response.readEntity(String.class);
		} else if (response.getStatus() == Response.Status.NO_CONTENT
				.getStatusCode()) {
			
		} else {

		}

	}

	public String getMapping() {
		return mapping;
	}

}

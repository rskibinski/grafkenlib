package pl.edu.agh.grafken.client.commands;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;

import pl.edu.agh.grafken.client.App;

public class PostDatabaseConfigurationCommand extends AbstractCommand implements
		ICommand {

	private Scanner in;

	public PostDatabaseConfigurationCommand(Scanner scanner) {
		this.in = scanner;
	}

	@Override
	public void execute() {
		System.out.println("Please enter filepath with new configuration");
		String confPath = in.nextLine().trim();
		try {
			String newConf = FileUtils.readFileToString(new File(confPath));
			WebTarget target = App.getInstance().getClient()
					.target(App.getBaseURI()).path("databases")
					.path("reloadConfiguration");
			Response response = target.request(MediaType.TEXT_PLAIN).post(
					Entity.entity(newConf, MediaType.TEXT_PLAIN));
			if (response.getStatus() == Response.Status.OK.getStatusCode()) {
				System.out
						.println("Configuration reloaded sucessfully, service is restarting");
			} else {
				System.out.println("New configuration was rejected by server");
			}

		} catch (IOException e) {
			System.out
					.println("File doesn't exist or cannot be read into configuration");
			return;
		}

	}

}

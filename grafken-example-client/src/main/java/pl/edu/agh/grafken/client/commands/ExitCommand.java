package pl.edu.agh.grafken.client.commands;


public class ExitCommand extends AbstractCommand {

	@Override
	public void execute() {
		System.exit(0);
	}

}

package pl.edu.agh.grafken.client;

import java.io.InputStreamReader;
import java.util.Scanner;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.UriBuilder;

import pl.edu.agh.grafken.client.menu.TopMenu;

public class App implements UIApi{

	private static final String BASE_URI = "http://localhost:8080/grafken/";
	
	

	public static UriBuilder getBaseURI() {
		return UriBuilder.fromUri(BASE_URI);
	}

	public Client getClient() {
		return ClientBuilder.newClient();
	}


	private static App instance;

	public static void main(String[] args) {
		instance = new App();
		instance.repl();
	}
	
	public void repl() {
		TopMenu topMenu = new TopMenu(new Scanner(new InputStreamReader(
				System.in)));
		while (true)
			topMenu.takeControl(this);
	}

	public static App getInstance() {
		if(instance == null){
			instance = new App();
		}
		return instance;
	}


}

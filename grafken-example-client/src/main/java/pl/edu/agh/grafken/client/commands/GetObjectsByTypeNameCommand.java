package pl.edu.agh.grafken.client.commands;

import java.util.InputMismatchException;
import java.util.Scanner;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import pl.edu.agh.grafken.client.App;
import pl.edu.agh.grafken.client.Clipboard;
import pl.edu.agh.grafken.lib.utils.PrintUtils;
import pl.edu.agh.grafken.server.wrapper.ObjectsCacheWrapper;

public class GetObjectsByTypeNameCommand extends AbstractCommand implements
		ICommand {

	private String databaseId;
	private Scanner in;

	public GetObjectsByTypeNameCommand(Scanner scanner, String databaseName) {
		this.in = scanner;
		this.databaseId = databaseName;
	}

	@Override
	public void execute() {
		System.out.println("Please enter object name:(or /abort to abort)");
		new PrintMappedObjectTypesCommand(databaseId, false).execute();
		String objectName = in.nextLine().trim();
		if (objectName.equals("/abort")) {
			return;
		}
		System.out
				.println("Please enter how many objects you'd like to retrieve(default 50)");
		int quantity;
		try {
			quantity = in.nextInt();
			if (quantity <= 0) {
				throw new InputMismatchException();
			}
		} catch (InputMismatchException e) {
			System.out.println("Wrong number, setting to default: 50");
			quantity = 50;
		}
		WebTarget target = App.getInstance().getClient()
				.target(App.getBaseURI()).path("API").path(databaseId)
				.path("getObjectData").path(objectName)
				.path(Integer.toString(quantity));
		Response response = target.request(MediaType.APPLICATION_JSON).get();

		ObjectsCacheWrapper results = response
				.readEntity(ObjectsCacheWrapper.class);

		PrintUtils.prettyPrintCache(results);
		Clipboard.caches.put(objectName, results);

	}

}

package pl.edu.agh.grafken.client.menu;

import java.util.Scanner;

import pl.edu.agh.grafken.client.commands.GenerateMappingFileCommand;
import pl.edu.agh.grafken.client.commands.MenuCommand;
import pl.edu.agh.grafken.client.commands.communication.GetDatabasesIdsCommand;

public class ChooseDatabaseMenu extends AbstractMenu {

	public ChooseDatabaseMenu(Scanner scanner) {
		super(scanner);
		Integer i = 1;
		GetDatabasesIdsCommand idsCommand = new GetDatabasesIdsCommand();
		idsCommand.execute();
		for (String dbId : idsCommand.getIdList()) {
			entries.put(i, new MenuEntry(dbId, i, new MenuCommand(new MappingOptionsMenu(scanner, dbId))));
//			entries.put(i, new MenuEntry(dbId, i, new MenuCommand(
//					new MappingOptionsMenu(scanner, dbId))));
//			i++;
		}

	}
	public class MappingOptionsMenu extends AbstractMenu {

		public MappingOptionsMenu(Scanner scanner, String dbId) {
			super(scanner);
			entries.put(1, new MenuEntry("Generate empty mapping file", 1,
					new GenerateMappingFileCommand(scanner, dbId)));
//			 entries.put(2, new MenuEntry("Load mapping file", 2,
//					 new LoadMappingFileCommand(scanner, db)));
		}

	}
}

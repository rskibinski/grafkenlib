package pl.edu.agh.grafken.client.commands;

import java.util.Scanner;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import pl.edu.agh.grafken.client.App;
import pl.edu.agh.grafken.client.Clipboard;
import pl.edu.agh.grafken.lib.utils.PrintUtils;
import pl.edu.agh.grafken.server.wrapper.ObjectsCacheWrapper;
import pl.edu.agh.grafken.server.wrapper.UserObjectWrapper;

public class GetObjectDependencyCommand implements ICommand {

	private Scanner in;
	private String mappingID;

	public GetObjectDependencyCommand(Scanner scanner, String mappingID) {
		this.in = scanner;
		this.mappingID = mappingID;
	}

	@Override
	public void execute() {
		new ShowClipboardCommand(in).execute();
		System.out.println("Enter object name to show stored objects");
		String metaName = in.nextLine().trim();
		ObjectsCacheWrapper clipboard = Clipboard.caches.get(metaName);
		if (clipboard == null || clipboard.getAllObjects().isEmpty()) {
			System.out.println("There is no info about given object.");
		} else {
			PrintUtils.prettyPrintCache(clipboard);
			int max = clipboard.getAllObjects().size() - 1;
			System.out.println("\n Enter object index (0-" + max + ")");
			// TODO: range check
			UserObjectWrapper object = clipboard.getAllObjects().get(Integer.parseInt(in.nextLine()));
			System.out.println("Enter reference name to query for");
			for (String ref : object.getMetaInformation()
					.getConnectedObjectsNames()) {
				System.out.println(ref);
			}
			String referenceName = in.nextLine().trim();
			
			WebTarget target = App.getInstance().getClient()
					.target(App.getBaseURI()).path("API").path(mappingID)
					.path("getObjectConnection").path(metaName).path(object.getKey().toString()).path(referenceName);
			Response response = target.request(MediaType.APPLICATION_JSON).get();
			ObjectsCacheWrapper results = response.readEntity(ObjectsCacheWrapper.class);
			System.out.println("RESULTS");
			PrintUtils.prettyPrintCache(results);
		}
	}

}

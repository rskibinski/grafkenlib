package pl.edu.agh.grafken.client.menu;

import java.util.Scanner;

import pl.edu.agh.grafken.client.commands.ExitCommand;
import pl.edu.agh.grafken.client.commands.MenuCommand;
import pl.edu.agh.grafken.client.commands.ShowClipboardCommand;

public class TopMenu extends AbstractMenu {
	public TopMenu(Scanner scanner) {
		super(scanner);
		entries.put(0, new MenuEntry("Exit", 0, new ExitCommand()));
		entries.put(1, new MenuEntry("Administration", 1, new MenuCommand(new AdministrationMenu(scanner))));
		entries.put(2, new MenuEntry("User access", 2, new MenuCommand(new UserMenu(scanner))));
		entries.put(3, new MenuEntry("Object clipboard", 3, new ShowClipboardCommand(scanner)));
	}

}

package pl.edu.agh.grafken.client.menu;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import pl.edu.agh.grafken.client.UIApi;

public abstract class AbstractMenu implements IMenu {
	protected Map<Integer, MenuEntry> entries;

	protected Scanner scanner;

	protected String input;
	
	protected UIApi appContext;

	public AbstractMenu(Scanner scanner) {
		this.scanner = scanner;
		entries = new TreeMap<Integer, MenuEntry>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pl.edu.agh.grafken.menu.IMenu#printMenu()
	 */
	@Override
	public void printMenu() {
		System.out.println("########################");
		System.out.println("Choose an option:");
		for (MenuEntry entry : entries.values()) {
			System.out.println(entry.getEntryString());
		}
	}

	@Override
	public void takeControl(UIApi app) {
		this.appContext = app;
		boolean isValidInput = false;
		while (!isValidInput) {
			printMenu();
			input = scanner.nextLine();
			try {
				if (entries.containsKey(Integer.parseInt(input))) {
					isValidInput = true;
					entries.get(Integer.parseInt(input)).execute();	//TODO: check it, its weird
				} else {
					printWrongInputMessage();
				}
			} catch (NumberFormatException e) {
				printWrongInputMessage();
			}
		}
	}

	protected void printWrongInputMessage() {
		System.out.println("Bledna opcja, wybierz poprawny numer pozycji menu");
	}
	
	public UIApi getApplicationContext(){
		return appContext;
	}
}

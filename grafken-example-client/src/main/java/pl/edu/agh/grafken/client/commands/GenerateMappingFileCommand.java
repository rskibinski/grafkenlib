package pl.edu.agh.grafken.client.commands;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;

import pl.edu.agh.grafken.client.commands.communication.GetDatabaseEmtpyMappingCommand;

public class GenerateMappingFileCommand extends AbstractCommand {

	private String dbId;
	private Scanner in;

	public GenerateMappingFileCommand(Scanner scanner, String dbId) {
		this.in = scanner;
		this.dbId = dbId;
	}

	@Override
	public void execute() {
		System.out
				.println("Enter filepath to save configuration file(default .):");
		String path = in.nextLine().trim();
		if (path.isEmpty()) {
			path = "./" + dbId;
		}
		GetDatabaseEmtpyMappingCommand getDatabaseEmtpyMapping = new GetDatabaseEmtpyMappingCommand(
				dbId);
		getDatabaseEmtpyMapping.execute();
		String mapping = getDatabaseEmtpyMapping.getMapping();
		System.out.println("Mapping");
		System.out.println(mapping);
		try {
			FileUtils.writeStringToFile(new File(path), mapping);
		} catch (IOException e) {
			System.out
					.println("It was impossible to write mapping on given path due to IO error");
		}
	}

}

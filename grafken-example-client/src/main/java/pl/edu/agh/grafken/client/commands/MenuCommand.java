package pl.edu.agh.grafken.client.commands;

import pl.edu.agh.grafken.client.menu.IMenu;

public class MenuCommand extends AbstractCommand {
	private IMenu menu;

	public MenuCommand(IMenu menu) {
		this.menu = menu;
	}

	@Override
	public void execute() {
		menu.takeControl(menu.getApplicationContext());
	}

}

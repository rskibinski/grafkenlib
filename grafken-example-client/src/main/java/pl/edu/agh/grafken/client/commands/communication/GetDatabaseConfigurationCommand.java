package pl.edu.agh.grafken.client.commands.communication;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;

import pl.edu.agh.grafken.client.App;
import pl.edu.agh.grafken.client.commands.AbstractCommand;
import pl.edu.agh.grafken.client.commands.ICommand;

public class GetDatabaseConfigurationCommand extends AbstractCommand implements
		ICommand {
	private Scanner in;

	public GetDatabaseConfigurationCommand(Scanner scanner) {
		this.in = scanner;
	}

	@Override
	public void execute() {
		WebTarget target2 = App.getInstance().getClient()
				.target(App.getBaseURI()).path("databases")
				.path("getConfiguration");
		Response response = target2.request(MediaType.TEXT_PLAIN).get();
		if (response.getStatus() == Response.Status.OK.getStatusCode()) {
			System.out.println("Actual database configuration is:");
			String entity = response.readEntity(String.class);
			System.out.println(entity);
			System.out
					.println("\n Do you wish to save it in to local file? (y/_n_)");
			if (in.nextLine().trim().equals("y")) {
				System.out.println("Write path:");
				String path = in.nextLine().trim();
				try {
					FileUtils.writeStringToFile(new File(path), entity);
				} catch (IOException e) {
					System.out.println("Unable to write to given location.");
				}
			}

		} else {
			System.out
					.println("Unexpected error while retrieving database configuration");
		}
	}

}

package pl.edu.agh.grafken.client.commands;

import java.util.ArrayList;

import pl.edu.agh.grafken.client.commands.communication.GetMappingMetaInfoCommand;
import pl.edu.agh.grafken.server.wrapper.ObjectMetaInformationWrapper;

public class PrintMappedObjectTypesCommand extends AbstractCommand implements
		ICommand {

	private String databaseId;
	private boolean withDetails;

	public PrintMappedObjectTypesCommand(String databaseName,
			boolean withDetails) {
		this.databaseId = databaseName;
		this.withDetails = withDetails;
	}

	@Override
	public void execute() {
		GetMappingMetaInfoCommand metaInfoCommand = new GetMappingMetaInfoCommand(
				databaseId);
		metaInfoCommand.execute();
		ArrayList<ObjectMetaInformationWrapper> info = metaInfoCommand
				.getMappingMetaInfo();
		for (ObjectMetaInformationWrapper meta : info) {
			System.out.println(meta.getName());
			if (withDetails) {
				System.out.println("\tFields:");
				for (String fieldName : meta.getFieldsNames()) {
					System.out.println("\t\t" + fieldName);
				}
			}
			if(!(meta.getConnectedObjectsNames().isEmpty())){
				System.out.println("\tConnected objects:");
				for(String connectedObjectName: meta.getConnectedObjectsNames()){
					System.out.println("\t\t" + connectedObjectName);
				}
				
			}
		}
	}

}

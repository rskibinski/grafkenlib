package pl.edu.agh.grafken.client.commands;

import java.util.Scanner;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import pl.edu.agh.grafken.client.App;
import pl.edu.agh.grafken.lib.utils.PrintUtils;
import pl.edu.agh.grafken.server.wrapper.ObjectsCacheWrapper;

public class FullTextSearchCommand implements ICommand {

	private String mappingID;
	private Scanner in;

	public FullTextSearchCommand(Scanner scanner, String databaseName) {
		this.in = scanner;
		this.mappingID = databaseName;
	}

	@Override
	public void execute() {
		new PrintMappedObjectTypesCommand(mappingID, false).execute();
		System.out.println("Please enter type of objects that you'd like to search for:");
		String objectType = in.nextLine();
		System.out.println("Please enter search query(plain text, % - wildcard(many signs), _ - wildcard(single sign):");
		String query = in.nextLine();
		
		WebTarget target = App.getInstance().getClient()
				.target(App.getBaseURI()).path("API").path(mappingID)
				.path("search").path(objectType).path(query);
		Response response = target.request(MediaType.APPLICATION_JSON).get();
		ObjectsCacheWrapper results = response.readEntity(ObjectsCacheWrapper.class);
		System.out.println("RESULTS");
		PrintUtils.prettyPrintCache(results);
	}

}

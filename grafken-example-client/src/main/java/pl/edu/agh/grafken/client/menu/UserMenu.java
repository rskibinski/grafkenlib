package pl.edu.agh.grafken.client.menu;

import java.util.Scanner;

import pl.edu.agh.grafken.client.commands.FullTextSearchCommand;
import pl.edu.agh.grafken.client.commands.GetObjectDependencyCommand;
import pl.edu.agh.grafken.client.commands.GetObjectsByTypeNameCommand;
import pl.edu.agh.grafken.client.commands.MenuCommand;
import pl.edu.agh.grafken.client.commands.PrintMappedObjectTypesCommand;
import pl.edu.agh.grafken.client.commands.communication.GetDatabasesIdsCommand;

public class UserMenu extends AbstractMenu implements IMenu {

	public UserMenu(Scanner scanner) {
		super(scanner);
		int i = 1;
		GetDatabasesIdsCommand idsCommand = new GetDatabasesIdsCommand();
		idsCommand.execute();
		for (String dbId : idsCommand.getIdList()) {
			entries.put(i, new MenuEntry(dbId, i, new MenuCommand(
					new DatabaseMenu(scanner, dbId))));
			i++;
		}

	}

	public class DatabaseMenu extends AbstractMenu {

		public DatabaseMenu(Scanner scanner, String databaseName) {
			super(scanner);
			entries.put(1, new MenuEntry("Print defined objects with fields",
					1, new PrintMappedObjectTypesCommand(databaseName, true)));
			entries.put(2, new MenuEntry("Get objects by object name", 2,
					new GetObjectsByTypeNameCommand(scanner, databaseName)));
			 entries.put(3, new MenuEntry("Get object depedency", 3, new
			 GetObjectDependencyCommand(scanner, databaseName)));
			 entries.put(4, new MenuEntry("Search for objects", 4, new FullTextSearchCommand(scanner, databaseName)));
		}
	}

}

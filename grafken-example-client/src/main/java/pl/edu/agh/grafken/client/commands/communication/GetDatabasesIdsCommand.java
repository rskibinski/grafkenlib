package pl.edu.agh.grafken.client.commands.communication;

import java.util.ArrayList;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import pl.edu.agh.grafken.client.App;
import pl.edu.agh.grafken.client.commands.AbstractCommand;
import pl.edu.agh.grafken.client.commands.ICommand;

public class GetDatabasesIdsCommand extends AbstractCommand implements ICommand {

	private ArrayList<String> idList;

	@SuppressWarnings("unchecked")
	@Override
	public void execute() {
		WebTarget target = App.getInstance().getClient()
				.target(App.getBaseURI()).path("databases").path("getIdList");
		idList = target.request(MediaType.APPLICATION_JSON)
				.get(ArrayList.class);

	}

	public ArrayList<String> getIdList() {
		return idList;
	}

}

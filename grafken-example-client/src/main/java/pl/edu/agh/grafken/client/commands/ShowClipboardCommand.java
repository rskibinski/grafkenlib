package pl.edu.agh.grafken.client.commands;

import java.util.Scanner;

import pl.edu.agh.grafken.client.Clipboard;

public class ShowClipboardCommand implements ICommand {

	private Scanner in;

	public ShowClipboardCommand(Scanner in) {
		this.in = in;
	}

	@Override
	public void execute() {
		if (Clipboard.caches.keySet().isEmpty()) {
			System.out
					.println("Clipboard is empty, populate it with some data first!");
		} else {
			System.out.println("Stored object types are:");
			for (String objectID : Clipboard.caches.keySet()) {
				System.out.println(objectID);
			}
		}
	}

}

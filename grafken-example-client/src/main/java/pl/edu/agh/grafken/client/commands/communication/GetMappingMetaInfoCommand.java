package pl.edu.agh.grafken.client.commands.communication;

import java.util.ArrayList;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import pl.edu.agh.grafken.client.App;
import pl.edu.agh.grafken.client.commands.AbstractCommand;
import pl.edu.agh.grafken.client.commands.ICommand;
import pl.edu.agh.grafken.server.wrapper.ObjectMetaInformationWrapper;

public class GetMappingMetaInfoCommand extends AbstractCommand implements ICommand {

	private String databaseId;
	private ArrayList<ObjectMetaInformationWrapper> mappingMetaInfo;
	
	public GetMappingMetaInfoCommand(String databaseId) {
		this.databaseId = databaseId;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute() {
		WebTarget target = App.getInstance().getClient().target(App.getBaseURI()).path("API").path(databaseId).path("getObjectTypesList");
		Response response = target.request(MediaType.APPLICATION_JSON).get();
		mappingMetaInfo = response.readEntity(new GenericType<ArrayList<ObjectMetaInformationWrapper>>(){});
	}
	
	public ArrayList<ObjectMetaInformationWrapper> getMappingMetaInfo(){
		return mappingMetaInfo;
	}

}

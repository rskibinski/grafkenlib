package pl.edu.agh.grafken.client.menu;

import java.util.Scanner;

import pl.edu.agh.grafken.client.commands.PostDatabaseConfigurationCommand;
import pl.edu.agh.grafken.client.commands.communication.GetDatabaseConfigurationCommand;

public class DatabaseConfigurationMenu extends AbstractMenu{

	public DatabaseConfigurationMenu(Scanner scanner) {
		super(scanner);
		entries.put(1, new MenuEntry("Load configuration from file", 1, new PostDatabaseConfigurationCommand(scanner)));
		entries.put(2, new MenuEntry("Dump configuration", 2, new GetDatabaseConfigurationCommand(scanner)));
	}


}

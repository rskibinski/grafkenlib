package pl.edu.agh.grafken.client.menu;

import pl.edu.agh.grafken.client.UIApi;

public interface IMenu {

	public void printMenu();

	public abstract void takeControl(UIApi applicationContext);
	
	public abstract UIApi getApplicationContext();

}
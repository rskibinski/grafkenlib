package pl.edu.agh.grafken.client;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import pl.edu.agh.grafken.lib.utils.PrintUtils;
import pl.edu.agh.grafken.server.wrapper.ObjectsCacheWrapper;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testGetPerson()
    {
		WebTarget target = App.getInstance().getClient()
				.target(App.getBaseURI()).path("API").path("Northwind")
				.path("getObjectData").path("MyPerson")
				.path(Integer.toString(20));
		Response response = target.request(MediaType.APPLICATION_JSON).get();
		ObjectsCacheWrapper results = response.readEntity(ObjectsCacheWrapper.class);
		
		PrintUtils.prettyPrintCache(results);
		
    }
}
